const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

module.exports = {
  isValidCommunity: Joi.object().keys({
    courseId: Joi.objectId().required(),
    chapterId: Joi.objectId().required(),
    lessonId: Joi.objectId().required(),
    studentId: Joi.objectId().required(),
    studentName: Joi.string().required(),
    teacherId: Joi.objectId().required(),
    teacherName: Joi.string().required(),
    question: Joi.object().keys({
      date: Joi.date().default(Date.now()),
      title: Joi.string(),
      attachments: Joi.array()
      // .items(
      //   Joi.object({
      //     type: Joi.string().valid(["Text", "Attachment", "Link", "Voice"]),
      //     value: Joi.string(),
      //   })
      // ),
    }),
  }),

  isValidReplay: Joi.object().keys({
    date: Joi.date().default(Date.now()),
    from: Joi.string().required().valid(["Teacher", "Student"]),
    title: Joi.string(),
    attachments: Joi.array().items(
      Joi.object({
        type: Joi.string().valid(["Text", "Attachment", "Link", "Voice"]),
        value: Joi.string(),
      })
    ),
  }),

  isValidMultipleReplies: Joi.object().keys({
    replies: Joi.array()
      .min(1)
      .required()
      .items(
        Joi.object({
          date: Joi.date().default(Date.now()),
          from: Joi.string().required().valid(["Teacher", "Student"]),
          title: Joi.string(),
          attachments: Joi.array().items(
            Joi.object({
              type: Joi.string().valid(["Text", "Attachment", "Link", "Voice"]),
              value: Joi.string(),
            })
          ),
          // attachments: Joi.array().items(
          //   Joi.object({
          //     type: Joi.string().valid(["Text", "Attachment", "Link", "Voice"]),
          //     value: Joi.string(),
          //   })
          // ),
        })
      ),
    toDelete: Joi.array(),
  }),
  isValidSearch: Joi.object().keys({
    courseId: Joi.objectId().required(),
    question: Joi.string().required(),
  }),
};
