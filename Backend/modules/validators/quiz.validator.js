const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

module.exports = {
  isValidQuiz: Joi.object().keys({
    lessonId: Joi.objectId().required(),
    quizId: Joi.objectId().required(),
    studentId: Joi.objectId().required(),
    teacherId: Joi.objectId().required(),
    quizName: Joi.string().required(),
    courseName: Joi.string().required(),
    answers: Joi.array()
      .min(1)
      .required()
      .items(
        Joi.object({
          questionId: Joi.objectId().required(),
          answerId: Joi.objectId().required(),
        })
      ),
    score: Joi.number(),
  }),
};