const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

module.exports = {
  isValidCard: Joi.object().keys({
    studentId: Joi.objectId().required(),
    course: Joi.objectId().required(),
    chapter: Joi.objectId(),
    lesson: Joi.objectId(),
    type: Joi.string().valid(["Course", "Lesson"]).required(),

  }),
};
