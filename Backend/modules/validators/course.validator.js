const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

module.exports = {
  isValidCourse: Joi.object().keys({
    name: Joi.string().required(),
    nameAr: Joi.string().required(),
    hidden: Joi.boolean().default(false),
    subjectId: Joi.string().required(),
    gradeId: Joi.objectId(),
    educationSystemId: Joi.objectId().required(),
    price: Joi.number(),
    pricePerLesson: Joi.number(),
    courseIcon: Joi.string().required(),
    teacherId: Joi.objectId(),
    prerequisities: Joi.array(),
    language: Joi.string(),
    courseDescription: Joi.string(),
    whatYouWillLearn: Joi.string(),
    courseIntro: Joi.string().required(),
    product: Joi.string(),
    isDeleted: Joi.boolean().default(false),
    date: Joi.date().default(Date.now()),
    exclusive: Joi.boolean().default(false),
    courseType: Joi.string().valid(["Educations", "Skills"]).required(),
    status: Joi.string().valid(["Recorded", "Group", "Private"]).required(),
  }),
};
