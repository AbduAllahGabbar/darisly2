const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

module.exports = {
  isValidNotification: Joi.object().keys({
    users: Joi.array().items(
      Joi.object({
        notificId: Joi.number(),
        userId: Joi.objectId(),
        type: Joi.string().required().valid(["Teacher", "Student"]),
        send: Joi.boolean().default(false),
      })
    ),
    notificAr: Joi.string().required(),
    notificEn: Joi.string().required(),
    reference: Joi.object(),
    date: Joi.date().default(Date.now()),
  }),
};
