const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const communityModel = require("../models/community.model");
const handleFiles = require("../../helpers/handleFiles");
const attachmentPath = require("../../helpers/attachmentPath.json");
const path = attachmentPath.attachments.communityAttachmentPath;

findAll = (req, res) => {
  let obj = {};
  callCommunityAggregateFunction(req, res, obj);
};
findAllCommunitiesWithCourseId = (req, res, courseId) => {
  let obj = {
    courseId: ObjectId(courseId),
  };
  if (req.query.filter === "solved") {
    obj = { ...obj, "replies.0": { $exists: true } };
  } else if (req.query.filter === "unsolved") {
    obj = { ...obj, "replies.0": { $exists: false } };
  }
  callCommunityAggregateFunction(req, res, obj);
};
callCommunityAggregateFunction = (req, res, obj) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  communityModel.defaultSchema
    .aggregate([
      {
        $match: obj,
      },
      {
        $lookup: {
          from: "students",
          localField: "studentId",
          foreignField: "_id",
          as: "student",
        },
      },
      {
        $unwind: {
          path: "$student",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: "$_id",
          courseId: { $first: "$courseId" },
          chapterId: { $first: "$chapterId" },
          lessonId: { $first: "$lessonId" },
          question: { $first: "$question" },
          attachments: { $first: "$attachments" },
          studentId: { $first: "$studentId" },
          studentFirstName: { $first: "$student.firstName" },
          studentLastName: { $first: "$student.lastName" },
          replies: { $first: "$replies" },
        },
      },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => res.json(err || data));
};
addRepliesIntoCommunity = async (req, res, id) => {
  if (req.body.attachments && req.body.attachments.length > 0) {
    for (const attachment of req.body.attachments) {
      if (
        attachment.type == "Attachment" &&
        attachment.value.startsWith("data:")
      ) {
        try {
          attachment.value = await handleFiles.saveFiles(
            attachment.value,
            "Attachment",
            path
          );
        } catch (error) {
          return res.status(500).send(error);
        }
      }
      // if (allAttachments.length === req.body.replies.length)
    }
  }
  // if (req.body.type === "Attachment") {
  //   try {
  //     req.body.value = await handleFiles.saveFiles(
  //       req.body.value,
  //       "Attachment",
  //       path
  //     );
  //   } catch (error) {
  //     return res.status(500).send(error);
  //   }
  // }
  communityModel.defaultSchema.findByIdAndUpdate(
    id,
    { $push: { replies: req.body } },
    {
      // While Update: show last updated document with new values
      new: true,
      // While Update: the default values will inserted without passing values explicitly
      setDefaultsOnInsert: true,
    },
    function (err, data) {
      if (err) res.status(500).send(err);
      else if (data === null) res.status(404).send("ID is not found");
      else res.status(200).send(data);
    }
  );
};
addMultipleAnswersIntoCommunity = async (req, res, id) => {
  let promise = new Promise(async (resolve, reject) => {
    if (req.body.replies && req.body.replies.length > 0) {
      for (const replay of req.body.replies) {
        if (replay.attachments && replay.attachments.length > 0) {
          for (const attachment of replay.attachments) {
            if (
              attachment.type == "Attachment" &&
              attachment.value.startsWith("data:")
            ) {
              try {
                attachment.value = await handleFiles.saveFiles(
                  attachment.value,
                  "Attachment",
                  path
                );
              } catch (error) {
                return res.status(500).send(error);
              }
            }
            // if (allAttachments.length === req.body.replies.length)
            resolve();
          }
        }
      }
    } else resolve();
  });
  promise.then(() => {
    communityModel.defaultSchema.findByIdAndUpdate(
      id,
      { $addToSet: { replies: req.body.replies } },
      {
        // While Update: show last updated document with new values
        new: true,
        // While Update: the default values will inserted without passing values explicitly
        setDefaultsOnInsert: true,
      },
      function (err, data) {
        if (err) res.status(500).send(err);
        else if (data === null) res.status(404).send("ID is not found");
        else res.status(200).send(data);
      }
    );
  });
};
updateCommunityAnswer = async (req, res, id) => {
  if (req.body.toDelete && req.body.toDelete.length > 0) {
    for (const toDelete of req.body.toDelete) {
      await handleFiles.deleteFile(toDelete, path);
    }
  }
  if (req.body.replies) {
    addMultipleAnswersIntoCommunity(req, res, id);
  }
};
search = (req, res) => {
  let obj = {
    courseId: ObjectId(req.body.courseId),
    "question.title": { $regex: req.body.question, $options: "i" },
  };
  callCommunityAggregateFunction(req, res, obj);
};
createCommunity = async (req, res) => {
  let promise = new Promise(async (resolve, reject) => {
    if (
      req.body.question.attachments &&
      req.body.question.attachments.length > 0
    ) {
      for (const attach of req.body.question.attachments) {
        if (attach.type == "Attachment" && attach.value.startsWith("data:")) {
          attach.value = await handleFiles.saveFiles(
            attach.value,
            "Attachment",
            path
          );
        }
      }

      resolve();
    } else {
      resolve();
    }
  });
  promise.then(() => {
    communityModel.defaultSchema.create(req.body, function (err, data) {
      if (err) res.status(500).send(err);
      else res.status(201).send(req.body);
    });
  });
};
deleteCommunity = async (req, res, id) => {
  if (req.body.toDelete && req.body.toDelete.length > 0) {
    for (const toDelete of req.body.toDelete) {
      await handleFiles.deleteFile(toDelete, path);
    }
  }
  communityModel.defaultSchema.findByIdAndRemove(id, function (err, data) {
    if (err) res.status(500).send(err);
    else {
      if (data === null) res.sendStatus(404);
      else res.sendStatus(200);
    }
  });
};
module.exports = {
  deleteCommunity,
  updateCommunity: communityModel.genericSchema.update,
  findById: communityModel.genericSchema.findById,
  create: createCommunity,
  findAll,
  addRepliesIntoCommunity,
  addMultipleAnswersIntoCommunity,
  findAllCommunitiesWithCourseId,
  updateCommunityAnswer,
  search,
};
