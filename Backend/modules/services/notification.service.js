const notificationModel = require("../models/notification.model");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

createCustom = (body) => {
  notificationModel.defaultSchema.create(body, function (err, result) {
    // if (err) res.status(500).send(err);
    // else res.status(201).send(result);
  });
};

getNotificationByUserId = (req, res, userId) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  const foundNotic = lang === "en" ? "notificEn" : "notificAr";

  notificationModel.defaultSchema
    .aggregate([
      {
        $match: {'users.userId': ObjectId(userId)},
      },
      {
        $group: {
          _id: "$_id",
          notification: { $first: `$${foundNotic}` },
          reference: { $first: "$reference" },
          date: { $first: "$date" },
        },
      },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => res.json(err || data));


  // notificationModel.defaultSchema
  //   .find({  'users':userId })
  //   .skip((pageNumber - 1) * pageSize)
  //   .limit(pageSize)
  //   .select({ notification: 1 })
  //   .sort({ date: -1 })
  //   .exec((err, data) => res.json(err || data));
};

callbackGetNotificationByUserId = (userId) => {
  return new Promise((resolve, reject) => {
    notificationModel.defaultSchema
      .find({ userId })
      .select({ notification: 1 })
      .sort({ date: -1 })
      .exec((err, data) => {
        if (err) reject(err);
        else resolve(data);
      });
  });
};

module.exports = {
  deleteNotification: notificationModel.genericSchema.delete,
  updateNotification: notificationModel.genericSchema.update,
  findById: notificationModel.genericSchema.findById,
  create: notificationModel.genericSchema.create,
  findAll: notificationModel.genericSchema.findAll,
  getNotificationByUserId,
  createCustom,
  callbackGetNotificationByUserId,
};