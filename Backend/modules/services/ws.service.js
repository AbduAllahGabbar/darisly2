module.exports = function () {
  const notifacationsModel = require("../models/notification.model");
  const mongoose = require("mongoose");
  const ObjectId = mongoose.Types.ObjectId;

  notifacationsModel.wsList = [];
  notifacationsModel.sendMessage = function(msg){
    notifacationsModel.wsList.forEach(s => {
      s.ws.send(JSON.stringify(msg))
    });
  }

  var express = require('express');
  var app = express();
  var expressWs = require('express-ws')(app);
  
  app.use(function (req, res, next) {
    console.log('middleware');
    req.testing = 'testing';
    return next();
  });
  
  app.get('/', function(req, res, next){
    console.log('get route', req.testing);
    res.end();
  });
  
  app.ws('/', function(ws, req) {

    ws.send(
      JSON.stringify({
        type: "ready"
      })
    );

    notifacationsModel.wsList.push({ws : ws})

    ws.on('message', function(data) {

     try {
      let message = JSON.parse(data);

      if (message.type == "notifications") {

            notifacationsModel.defaultSchema
              .find({ "users.userId": ObjectId(message.userId) })
              .limit(10)
              .sort({ date: -1 })
              .exec((err, data) => {
                let arr = [];
                data.forEach((_data) => {
                  _data.users.forEach((_u) => {
                    if (_u.userId.toString() === message.userId.toString() && !_u.send) {
                      arr.push({
                        ar :_data.notificAr,
                        en :_data.notificEn,
                        reference : _data.reference,
                        date : _data.date
                      });
                      _u.send = true;
                    }
                  });
                  notifacationsModel.defaultSchema.updateOne({ _id: ObjectId(_data._id) },_data,function (err, data) {
                  });
                });
                console.log(arr);
                ws.send(
                  JSON.stringify({
                    type: "notifications",
                    list: arr,
                  })
                );
                
              });
          
      
      }
    } catch (err) {
      console.log(err)
    }
  

    });
    console.log('socket', req.testing);
  });
  
  app.listen(3002);



  // let WS = new WebSocket.Server({
  //   noServer: true,
  // });



  // let onlineUsers = [];


  // app.on("upgrade", function upgrade(request, socket, head) {
  //   console.log("eeeeeeeeeeeeeeeeeeeeeeeeeeee");
  //   const pathname = parent.url.parse(request.url).pathname;
  //   // if (pathname === "/api/ws") {
  //     WS.handleUpgrade(request, socket, head, function done(ws) {
  //       WS.emit("connection", ws, request);
  //     });
  //   // }
  // });

  // WS.on("connection", (ws, req) => {

  //   ws.on('message', message => console.log(message));
    
  //   // first time
  //   let newUser = {
  //     ws: ws,
  //     id: onlineUsers.length + 1,
  //   };
  //   onlineUsers.push(newUser);

  //   ws.send(
  //     JSON.stringify({
  //       type: "connected",
  //       userID: newUser.id,
  //     })
  //   );
  // });

  // WS.on("message", (data) => {
  //   // others time
  //   try {
  //     let message = JSON.parse(data);

  //     if (message.type == "info") {
  //       onlineUsers.forEach((user) => {
  //         if (user.id == message.userID) {
  //           user.info = message.info;
  //           // send old notification
  //           // findmany from notifications
  //           notifacationsModel.defaultSchema
  //             .find({ "users.id": user.id })
  //             .limit(10)
  //             .sort({ date: -1 })
  //             .exec((err, data) => {
  //               let arr = [];
  //               data.forEach((_data) => {
  //                 _data.users.forEach((_u) => {
  //                   if (_u.id === user.id && !_u.send) {
  //                     arr.push(_data.notification);
  //                     _u.send = true;
  //                     notifacationsModel.defaultSchema.updateOne(_data);
  //                   }
  //                 });
  //               });

  //               user.ws.send(
  //                 JSON.stringify({
  //                   type: "notifications",
  //                   list: arr,
  //                 })
  //               );
  //             });
  //         }
  //       });
  //     }
  //   } catch (EX) {}
  // });

  // setInterval(() => {
  //   // find many notifications
  //   notifacationsModel.defaultSchema
  //     .find()
  //     .limit(10)
  //     .sort({ date: -1 })
  //     .exec((err, data) => {
  //       data.forEach((_data) => {
  //         _data.usersId.forEach((_userId) => {
  //           onlineUsers.forEach((onlineUser) => {
  //             if (_userId == onlineUser.id) {
  //               onlineUser.ws.send(
  //                 JSON.stringify({
  //                   type: "notifications",
  //                   list: [],
  //                 })
  //               );
  //             }
  //           });
  //         });
  //       });
  //     });
  // }, 1000 * 60);

  // return {
  //   ws: WS,
  //   onlineUsers: onlineUsers,
  // };
};
