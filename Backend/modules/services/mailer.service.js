const mailerModel = require("../models/mailer.model");

createUser = (obj) => {
  mailerModel.defaultSchema.create(obj, function (err, doc) {});
};

create = (req, res) => {
  mailerModel.defaultSchema.create(req.body, function (err, result) {
    if (err) res.status(500).send(err);
    else res.status(201).send(result);
  });
};

findMailer = (obj, res, type) => {
  return new Promise((resolve, reject) => {
    mailerModel.defaultSchema
      .findOne({
        email: obj.email,
        userType: obj.userType,
        type: obj.type,
        code: obj.code,
      })
      .exec((err, data) => {
        if (err) res.status(500).send(err);
        else {
          if (data) {
            if (type == "pass") {
              resolve(data);
            } else if (type == "send") {
              res.status(200).send(data);
            }
          } else if (type == "send") {
            res.status(404).send("Mailer Code Not Found");
          } else if (type == "pass") {
            resolve(null);
          }
        }
      });
  });
};

module.exports = {
  findMailer,
  createUser,
  create,
};
