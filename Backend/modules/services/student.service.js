const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const studentModel = require("../models/student.model");
const notificationService = require("../services/notification.service");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { query } = require("../../helpers/logging");

create = (req, res, result) => {
  studentModel.defaultSchema.create(req.body, function (err, doc) {
    if (err) res.status(500).send(err);
    else {
      result(doc);
      res.status(201).send(doc);
    }
  });
};

findStudent = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  let student = await studentModel.defaultSchema.findOne({ email });

  if (!student) res.status(400).send("Invalid email or password");
  else if (!student.verify) res.status(400).send("Email Is Not Verify");
  else {
    const validPassword = await bcrypt.compare(password, student.password);
    if (!validPassword)
      return res.status(400).send("Invalid email or password");
    else {
      const ONE_WEEK = 604800;
      //Generating the token
      const token = jwt.sign(
        {
          id: student._id,
        },
        process.env.SECRET,
        {
          expiresIn: ONE_WEEK,
        }
      );
      let newStudent = { password, ...student };
      delete newStudent._doc.password;

      return res.status(200).send({
        success: true,
        message: "You can login now",
        student: newStudent._doc,
        token,
      });
    }
  }
};
socialMediaLogin = async (req, res) => {
  const email = req.body.email;
  studentModel.defaultSchema.findOneAndUpdate(
    { email },
    { $set: { socialMediaToken: req.body.socialMediaToken } },
    {
      // While Update: show last updated document with new values
      new: true,
      // While Update: the default values will inserted without passing values explicitly
      setDefaultsOnInsert: true,
    },
    function (err, student) {
      if (err) res.status(500).send(err);
      else {
        if (!student) res.status(400).send("Invalid email");
        else {
          const ONE_WEEK = 604800;
          //Generating the token
          const token = jwt.sign(
            {
              id: student._id,
            },
            process.env.SECRET,
            {
              expiresIn: ONE_WEEK,
            }
          );
          let newStudent = { ...student };
          delete newStudent._doc.password;
          return res.status(200).send({
            success: true,
            message: "You can login now",
            student: newStudent._doc,
            token,
          });
        }
      }
    }
  );
};

findAllStudents = async (req, res) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  let students = await studentModel.defaultSchema
    .find()
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .populate(["educationSystem", "grade"])
    .select({ password: 0, __v: 0 });
  if (!students) res.status(500).send("No Students Found");
  else return res.status(200).send(students);
};

addPurchaseIntoStudent = async (req, res, courseIdsUnique, stuId, type) => {
  return new Promise((resolve, reject) => {
    // const lessonIds = req.body.lessons.map((e) => e.lessonId);
    const lang = req.headers.lang ? req.headers.lang : "en";
    const toFound = lang === "en" ? "name" : "nameAr";
    let query = { _id: ObjectId(stuId) };

    if (req.body.promoCode) {
      query = {
        _id: ObjectId(stuId),
        "myOrders.promoCode": { $ne: ObjectId(req.body.promoCode) },
      };
    }

    studentModel.defaultSchema.findOne(query, (err1, doc) => {
      if (err1) {
        reject(err1);
      } else {
        if (doc === null) {
          res.status(404).send("Promo Code used from before");
        } else {
          courseIdsUnique = courseIdsUnique || [];
          courseIdsUnique.forEach((_cIu) => {
            foundCourseId = false;
            doc.courses.forEach((_c) => {
              if (_c && _cIu && _c.toString() === _cIu.toString()) {
                foundCourseId = true;
              }
            });

            if (!foundCourseId) doc.courses.push(_cIu);
          });
          let orderLength = doc.myOrders.length + 1
          let orderCode = doc._id.toString().slice(0, 10) + orderLength;
          let myOrder = {
            code: orderCode,
            coursesId: courseIdsUnique,
            totalPrice: req.body.totalPrice || 0,
            netPrice: req.body.netPrice || 0,
            date: new Date(),
          };

          if (req.body.purchaseType) {
            myOrder.type = req.body.purchaseType;
            myOrder.purchaseCode = req.body.purchaseCode;

            if (req.body.promoCode) {
              myOrder.promoCode = req.body.promoCode;
            }
          } else if (req.body.my_order_code) {
            myOrder.type = req.body.my_order_code.type;
            myOrder.purchaseCode = req.body.my_order_code.purchaseCode;
          }

          if (req.body.packages && req.body.packages.length > 0) {
            myOrder.packages = req.body.packages;
          }

          doc.myOrders = doc.myOrders || [];

          if (!req.body.isSkill) {
            doc.myOrders.push(myOrder);
          }
          req.body.lessons.forEach((_reqL) => {
            let objNotific = {
              users: [{ userId: _reqL.teacherId, type: "Teacher" }],
              reference: {
                type: "bookLesson",
                purchaseType: myOrder.type,
                lessonId: _reqL._id,
              },
            };

            objNotific.notificEn = `The ( ${_reqL[toFound]} ) lesson was purchased from student ( ${doc.firstName} )`;
            objNotific.notificAr = `تم شراء درس ( ${_reqL[toFound]} ) من الطالب ( ${doc.firstName} )`;

            notificationService.createCustom(objNotific);

            let foundLessonId = false;

            doc.lessons.forEach((_docL) => {
              if (
                _reqL.lessonId &&
                _docL.lessonId &&
                _reqL.lessonId.toString() === _docL.lessonId.toString()
              ) {
                _docL.order = _docL.order || [];
                // _docL.packagesId = _docL.packagesId || []

                // let foundPackage = _docL.packagesId.some(_pI => _pI && _reqL.packageId && _pI.toString() === _reqL.packageId.toString())

                // if (!foundPackage && typeof _reqL.packageId === 'string') {
                //   _docL.packagesId.push(_reqL.packageId)
                // }

                _docL.order.push({ code: orderCode, price: _reqL.price });
                foundLessonId = true;
              }
            });

            if (!foundLessonId) {
              _reqL.order = [{ code: orderCode, price: _reqL.price }];

              // if (_reqL.packageId) {
              //   _reqL.packagesId = [_reqL.packageId]
              //   delete _reqL.packageId
              // }

              doc.lessons.push(_reqL);
            }
          });

          studentModel.defaultSchema.updateOne(
            { _id: ObjectId(doc._id) },
            doc,
            function (err, data) {
              if (err) res.status(500).send(err);
              else if (data === null)
                res
                  .status(404)
                  .send(
                    "Student ID is not found or Promo Code used from before"
                  );
              else {
                if (type === "other") {
                  resolve(data);
                } else {
                  if (data.nModified === 0) {
                    res
                      .status(200)
                      .send(
                        "No Data has Modified, Maybe there is a repeating in data"
                      );
                  } else {
                    res.status(200).send("Data has Modified");
                  }
                }
              }
            }
          );
        }
      }
    });
  });
};

findMyOrders = async (req, res, courses) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  let matchObj = {};
  if (req.body.studentId) {
    matchObj._id = ObjectId(req.body.studentId);
  }

  studentModel.defaultSchema
    .aggregate([
      {
        $match: matchObj,
      },
      { $sort: { date: -1 } },
    ])
    .skip((pageNumber - 1) * pageSize)
    .exec((err, data) => {
      if (err) res.status(500).send(err);
      else if (data && data.length > 0) {
        let orders = [];
        let checker = (arr, target) =>
          target.every((v) => arr.includes(v.toString()));
        let coursesId = [];
        courses.forEach((_c) => {
          coursesId.push(_c._id.toString());
        });
        data.forEach((_student) => {
          if (_student.myOrders && _student.myOrders.length > 0) {
            _student.myOrders.forEach((_order) => {
              if (checker(coursesId, _order.coursesId)) {
                _order.studentId = _student._id;
                _order.phoneNumber = _student.phoneNumber;
                _order.firstName = _student.firstName;
                _order.lastName = _student.lastName;
                _order.lessons = _student.lessons;
                orders.push({ ..._order });
              }
            });
          }
        });
        orders.forEach((_order) => {
          _order.courses = [];

          _order.lessons.forEach((_lesson) => {
            courses.forEach((_course) => {
              if (
                _lesson.courseId &&
                _course._id.toString() === _lesson.courseId.toString() &&
                _lesson.order.some((_o) => _o.code === _order.code)
              ) {
                let course = {
                  courseId: _course._id,
                  courseName: _course.courseName,
                  courseType: _course.courseType,
                  name: _course.name,
                  price: _course.price,
                  subjectName: _course.subject,
                  chapters: [],
                };

                _course.chapters.forEach((_chapter) => {
                  let chapter = {
                    _id: _chapter._id,
                    name: _chapter[toFound],
                    lessons: [],
                  };

                  _chapter.lessons = _chapter.lessons || [];

                  _chapter.lessons.forEach((_l) => {
                    if (
                      _lesson.lessonId &&
                      _l._id &&
                      _lesson.lessonId.toString() === _l._id.toString()
                    ) {
                      lessonObj = {
                        _id: _l._id,
                        name: _l[toFound],
                        price: _l.price,
                        liveType: _l.liveType,
                        validFor: _l.validFor,
                        startDate: _l.startDate,
                        endDate: _l.endDate,
                      };

                      // _order.packages.forEach(_p => {
                      //   if (
                      //     _lesson.packagesId.some(_pId => _p.packageId
                      //       && _pId.toString() === _p.packageId.toString())
                      //   )
                      //     lessonObj.packageId = _p.packageId
                      // });

                      _order.packages.forEach((_p) => {
                        if (
                          _p.courseId.toString() === _lesson.courseId.toString()
                        ) {
                          lessonObj.packageId = _p._id;
                        }
                      });

                      chapter.lessons.push(lessonObj);
                    }
                  });

                  if (chapter.lessons.length > 0) {
                    course.chapters.push(chapter);
                  }
                });

                if (
                  !_order.courses.some(
                    (_c) =>
                      _c.courseId &&
                      _lesson.courseId &&
                      _c.courseId.toString() === _lesson.courseId.toString()
                  )
                )
                  _order.courses.push(course);
              }
            });
          });
          delete _order.lessons;
        });
        res.status(200).send({ myorders: orders });
      } else {
        res.status(200).send("Data Not Found");
      }
    });
};

findMyOrdersAdmin = async (req, res, courses) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  let filter = { $match: {} };

  if (req.body.filter) {
    filter = {
      $match: {
        $or: [
          { firstName: { $regex: req.body.filter, $options: "i" } },
          { lastName: { $regex: req.body.filter, $options: "i" } },
          { phoneNumber: { $regex: req.body.filter, $options: "i" } },
          { 'myOrders.code': { $regex: req.body.filter, $options: "i" } },
          { 'myOrders.purchaseCode': { $regex: req.body.filter, $options: "i" } },
        ],
      },
    };
  }
  studentModel.defaultSchema
    .aggregate([
      filter,
      { $sort: { date: -1 } },
    ])
    .skip((pageNumber - 1) * pageSize)
    .exec((err, data) => {
      if (err) res.status(500).send(err);
      else if (data && data.length > 0) {
        let orders = [];
        let checker = (arr, target) =>
          target.every((v) => arr.includes(v.toString()));
        let coursesId = [];
        courses.forEach((_c) => {
          coursesId.push(_c._id.toString());
        });
        data.forEach((_student) => {
          if (_student.myOrders && _student.myOrders.length > 0) {
            let found = false;
            _student.myOrders.forEach((_order) => {
              if(req.body.filter && (_order.purchaseCode == req.body.filter || _order.code == req.body.filter)) {
                found = true
                 if (checker(coursesId, _order.coursesId)) {
                  _order.studentId = _student._id;
                  _order.phoneNumber = _student.phoneNumber;
                  _order.firstName = _student.firstName;
                  _order.lastName = _student.lastName;
                  _order.lessons = _student.lessons;
                  orders.push({ ..._order });
                }
              }
            });
            if(!found){
              _student.myOrders.forEach((_order) => {
                   if (checker(coursesId, _order.coursesId)) {
                    _order.studentId = _student._id;
                    _order.phoneNumber = _student.phoneNumber;
                    _order.firstName = _student.firstName;
                    _order.lastName = _student.lastName;
                    _order.lessons = _student.lessons;
                    orders.push({ ..._order });
                  }
              });
            }
          }
        });
        orders.forEach((_order) => {
          _order.courses = [];
          _order.lessons.forEach((_lesson) => {
            courses.forEach((_course) => {
              if (
                _lesson.courseId &&
                _course._id.toString() === _lesson.courseId.toString() &&
                _lesson.order.some((_o) => _o.code === _order.code)
              ) {
                let course = {
                  courseId: _course._id,
                  courseName: _course.courseName,
                  courseType: _course.courseType,
                  name: _course.name,
                  price: _course.price,
                  subjectName: _course.subject,
                  chapters: [],
                };

                _course.chapters.forEach((_chapter) => {
                  let chapter = {
                    _id: _chapter._id,
                    name: _chapter[toFound],
                    lessons: [],
                  };

                  _chapter.lessons = _chapter.lessons || [];

                  _chapter.lessons.forEach((_l) => {
                    if (
                      _lesson.lessonId &&
                      _l._id &&
                      _lesson.lessonId.toString() === _l._id.toString()
                    ) {
                      lessonObj = {
                        _id: _l._id,
                        name: _l[toFound],
                        price: _l.price,
                        liveType: _l.liveType,
                        validFor: _l.validFor,
                        startDate: _l.startDate,
                        endDate: _l.endDate,
                      };

                      // _order.packages.forEach(_p => {
                      //   if (
                      //     _lesson.packagesId.some(_pId => _p.packageId
                      //       && _pId.toString() === _p.packageId.toString())
                      //   )
                      //     lessonObj.packageId = _p.packageId
                      // });

                      _order.packages.forEach((_p) => {
                        if (
                          _p.courseId.toString() === _lesson.courseId.toString()
                        ) {
                          lessonObj.packageId = _p._id;
                        }
                      });

                      chapter.lessons.push(lessonObj);
                    }
                  });

                  if (chapter.lessons.length > 0) {
                    course.chapters.push(chapter);
                  }
                });

                if (
                  !_order.courses.some(
                    (_c) =>
                      _c.courseId &&
                      _lesson.courseId &&
                      _c.courseId.toString() === _lesson.courseId.toString()
                  )
                )
                  _order.courses.push(course);
              }
            });
          });
          delete _order.lessons;
        });
        res.status(200).send({ myorders: orders });
      } else {
        res.status(200).send("Data Not Found");
      }
    });
};

findStudentsLessons = async (req, res, type) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;

    let searchCourseId = { $match: {} };
    if (req.body.courseId) {
      searchCourseId = {
        $match: { "lessons.courseId": { $in: [ObjectId(req.body.courseId)] } },
      };
    }

    if (req.body.endDate) {
      searchCourseId = { $match: { "lessons.endDate": { $gte: new Date() } } };
    }

    studentModel.defaultSchema
      .aggregate([
        { $unwind: "$lessons" },
        searchCourseId,
        {
          $group: {
            _id: "$_id",
            lessons: { $push: "$lessons" },
            myOrders: { $first: "$myOrders" },
            firstName: { $first: "$firstName" },
            lastName: { $first: "$lastName" },
            phoneNumber: { $first: "$phoneNumber" },
          },
        },
      ])
      .exec((err, data) => {
        if (err) {
          res.status(500).send(err);
        } else {
          if (type === "pass") {
            resolve(data);
          } else {
            res.status(200).send(data);
          }
        }
      });
  });
};

updateStudentLessonPurchase = async (req, res) => {
  return new Promise((resolve, reject) => {
    const date = new Date();
    const endDate = date.setDate(date.getDate() + req.body.validFor);
    studentModel.defaultSchema.findOne(
      {
        _id: ObjectId(req.body.studentId),
        "lessons.lessonId": req.body.lessonId,
      },
      (err, doc) => {
        if (!err && doc) {
          if (doc.lessons && doc.lessons.length > 0) {
            doc.lessons.forEach((_l) => {
              if (_l.lessonId.toString() === req.body.lessonId) {
                if (req.body.firstTimeLesson) {
                  _l.seen = true;
                  _l.validFor = req.body.validFor;
                  _l.startDate = new Date();
                  _l.endDate = endDate;
                  _l.watchedVideos = [ObjectId(req.body.videoId)];
                } else if (req.body.firstTimeVideo) {
                  _l.watchedVideos = _l.watchedVideos || [];
                  _l.watchedVideos.push(ObjectId(req.body.videoId));
                }
              }
            });

            studentModel.defaultSchema.updateOne(
              {
                _id: ObjectId(req.body.studentId),
                "lessons.lessonId": ObjectId(req.body.lessonId),
              },
              // {
              //   $set: {
              //     "lessons.$.seen": true,
              //     "lessons.$.validFor": req.body.validFor,
              //     "lessons.$.startDate": new Date(),
              //     "lessons.$.endDate": endDate,
              //   },
              // }
              // doc,
              // {
              //   // While Update: show last updated document with new values
              //   new: true,
              //   // While Update: the default values will inserted without passing values explicitly
              //   setDefaultsOnInsert: true,
              // }
              doc,
              function (err, data) {
                if (err) reject(err);
                else if (data === null) reject("ID is not found");
                else resolve("Data updated successfully");
              }
            );
          } else reject("LessonID is not found");
        } else resolve("ID is not found");
      }
    );
  });
};

findStudentCourses = async (req, res, id) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    studentModel.defaultSchema
      .find({ _id: ObjectId(id) })
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .select({ courses: 1, lessons: 1 })
      .exec((err, data) => {
        if (err) res.status(500).send(err);
        else {
          if (data && data.length > 0 && data[0].courses.length > 0) {
            resolve(data);
          } else {
            if (data && data.length > 0 && data[0].courses.length === 0) {
              res.status(200).send([]);
            } else {
              res.status(200).send(data);
            }
          }
        }
      });
  });
};

findStudentLessons = async (req, res, id, type) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;

    let searchCourseId = { $match: {} };
    if (req.body.courseId) {
      searchCourseId = {
        $match: { "lessons.courseId": { $in: [ObjectId(req.body.courseId)] } },
      };
    }

    studentModel.defaultSchema
      // .find(objFound)
      // .skip((pageNumber - 1) * pageSize)
      // .limit(pageSize)
      .aggregate([
        {
          $match: { _id: ObjectId(id) },
        },
        { $unwind: "$lessons" },
        searchCourseId,

        { $group: { _id: "$_id", lessons: { $push: "$lessons" } } },
      ])
      // .select({ lessons: 1 })
      .exec((err, data) => {
        if (err) res.status(500).send(err);
        else {
          if (type === "other") {
            resolve(data[0] || { lessons: [] });
          } else {
            res.status(200).send(data[0] || { lessons: [] });
          }
        }
      });
  });
};
addFavoriteIntoStudent = async (req, res, id) => {
  studentModel.defaultSchema.findByIdAndUpdate(
    id,
    {
      $addToSet: {
        favorites: req.body.courseId,
      },
    },
    {
      // While Update: show last updated document with new values
      new: true,
      // While Update: the default values will inserted without passing values explicitly
      setDefaultsOnInsert: true,
    },
    function (err, data) {
      if (err) res.status(500).send(err);
      else if (data === null) res.status(404).send("ID is not found");
      else res.status(200).send(data);
    }
  );
};

removeStudentFavorite = async (req, res, id, courseId) => {
  studentModel.defaultSchema.findByIdAndUpdate(
    id,
    { $pull: { favorites: courseId } },
    { safe: true, multi: true },
    function (err, data) {
      if (err) res.status(500).send(err);
      else if (data === null) res.status(404).send("ID is not found");
      else res.sendStatus(200);
    }
  );
};
findStudentFavorites = async (req, res, id) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    studentModel.defaultSchema
      .find({ _id: ObjectId(id) })
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .select({ favorites: 1 })
      .exec((err, data) => {
        if (err) res.status(500).send(err);
        else {
          if (data && data.length > 0 && data[0].favorites.length > 0) {
            resolve(data[0].favorites);
          } else {
            res.status(200).send([]);
          }
        }
      });
  });
};

checkStudentCourseInFavorite = async (req, res, studentId, courseId) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  studentModel.defaultSchema
    .find({ _id: ObjectId(studentId), favorites: { $in: ObjectId(courseId) } })
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .select({ favorites: 1 })
    .exec((err, data) => {
      if (err) res.status(500).send(err);
      else {
        if (data && data.length > 0) res.status(200).send("Found");
        else res.status(404).send("Not Found");
      }
    });
};

verifyStudent = async (req, res) => {
  const email = req.body.email;

  studentModel.defaultSchema.findOneAndUpdate(
    { email },
    { $set: { verify: true } },
    {
      new: true,
      setDefaultsOnInsert: true,
    },
    function (err, data) {
      if (err) res.status(500).send(err);
      else if (data === null) res.status(404).send("ID is not found");
      else res.status(200).send(data);
    }
  );
};

forgetPassword = async (req, res) => {
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return callback(err);
    }
    bcrypt.hash(req.body.newPassword, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      req.body.newPassword = hash;
      const email = req.body.email;
      studentModel.defaultSchema.findOneAndUpdate(
        { email },
        { $set: { password: req.body.newPassword } },
        {
          // While Update: show last updated document with new values
          new: true,
          // While Update: the default values will inserted without passing values explicitly
          setDefaultsOnInsert: true,
        },
        function (err, data) {
          if (err) res.status(500).send(err);
          else if (data === null) res.status(404).send("Email is not found");
          else res.status(200).send(data);
        }
      );
    });
  });
};

changePassword = async (req, res, id) => {
  let student = await studentModel.defaultSchema.findOne({ _id: id });
  if (!student) res.status(400).send("Invalid data");
  else {
    const validPassword = await bcrypt.compare(
      req.body.oldPassword,
      student.password
    );
    if (!validPassword) return res.status(400).send("Wrong old password");
    else {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) {
          return callback(err);
        }
        bcrypt.hash(req.body.newPassword, salt, (err, hash) => {
          if (err) {
            return next(err);
          }
          req.body.newPassword = hash;
          studentModel.defaultSchema.findByIdAndUpdate(
            id,
            { $set: { password: req.body.newPassword } },
            {
              // While Update: show last updated document with new values
              new: true,
              // While Update: the default values will inserted without passing values explicitly
              setDefaultsOnInsert: true,
            },
            function (err, data) {
              if (err) res.status(500).send(err);
              else if (data === null) res.status(404).send("ID is not found");
              else res.status(200).send(data);
            }
          );
        });
      });
    }
  }
};
findStudentById = (id) => {
  return new Promise(async (resolve, reject) => {
    const student = await studentModel.defaultSchema.findById(id);
    if (student) resolve(student);
    else reject(student);
  });
};

findPromoIntoStudent = async (req, res) => {
  return new Promise((resolve, reject) => {
    studentModel.defaultSchema.findOne(
      {
        _id: ObjectId(req.body.studentId),
        "myOrders.promoCode": ObjectId(req.body.promo),
      },
      // {
      //   $addToSet: {
      //     promoCodes: req.body.promoCode,
      //   },
      // },
      // {
      //   // While Update: show last updated document with new values
      //   new: true,
      //   // While Update: the default values will inserted without passing values explicitly
      //   setDefaultsOnInsert: true,
      // },
      function (err, data) {
        if (err) reject(err);
        else if (data === null) resolve(false);
        else resolve(true);
      }
    );
  });
};

updateSendNotific = async (studentId, lessonId) => {

  studentModel.defaultSchema.findOne(
    { _id : ObjectId(studentId) },
    function (err, student) {
      if(!err){
        student.lessons.forEach(_l => {
          if(_l.lessonId.toString() === lessonId.toString()){
            _l.sendNotific = true
          }
        });
        studentModel.defaultSchema.updateOne(
          { _id: ObjectId(student._id) },
          student,
          function (err, data) {
          }
        );
      }
     
    }
  );
 
};

module.exports = {
  deleteStudent: studentModel.genericSchema.delete,
  updateStudent: studentModel.genericSchema.update,
  findById: studentModel.genericSchema.findById,
  create,
  findAll: findAllStudents,
  findStudentAccount: findStudent,
  addPurchaseIntoStudent,
  findStudentCourses,
  findStudentLessons,
  findStudentsLessons,
  findMyOrdersAdmin,
  addFavoriteIntoStudent,
  findStudentFavorites,
  verifyStudent,
  forgetPassword,
  changePassword,
  updateSendNotific,
  updateStudentLessonPurchase,
  checkStudentCourseInFavorite,
  findStudentById,
  removeStudentFavorite,
  findMyOrders,
  socialMediaLogin,
  findPromoIntoStudent,
};
