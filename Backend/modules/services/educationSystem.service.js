const educationSystemModel = require("../models/educationSystem.model");

findAll = (req, res) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  educationSystemModel.defaultSchema
    .aggregate([
      {
        $match: req.body,
      },
      {
        $group: {
          _id: "$_id",
          name: { $first: `$${toFound}` },
          courseType: { $first: "$courseType" },
        },
      },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {
      res.json(err || data);
    });
};

getEducationSystemCoreById = (req, res, eduId) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 50;
  educationSystemModel.defaultSchema
    .findOne({ _id: eduId })
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {
      res.json(err || data);
    });
};

findAllWithCourseType = (req, res) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  educationSystemModel.defaultSchema
    .aggregate([
      {
        $match: req.params,
      },
      {
        $group: {
          _id: "$_id",
          name: { $first: `$${toFound}` },
          courseType: { $first: "$courseType" },
        },
      },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {
      res.json(err || data);
    });
};

module.exports = {
  deleteEducationSystem: educationSystemModel.genericSchema.delete,
  updateEducationSystem: educationSystemModel.genericSchema.update,
  findById: educationSystemModel.genericSchema.findById,
  create: educationSystemModel.genericSchema.create,
  findAll,
  findAllWithCourseType,
  getEducationSystemCoreById
};
