const mongoose = require("mongoose");
const cardModel = require("../models/card.model");
const ObjectId = mongoose.Types.ObjectId;

findAll = (req, res) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  let search = {};

    if (req.body.studentId) {
      search.studentId = ObjectId(req.body.studentId);
    }

  cardModel.defaultSchema
    .find(search)
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {

      if(err) res.json(err)
      else {

        let arr = []

        data.forEach(_d => {
          let obj = {
            _id : _d._id,
            studentId : _d.studentId,
            type : _d.type,
          }

          if(_d.type =='Lesson' && _d.lesson){
              obj.productId = _d.lesson.product            
              obj.lessonId = _d.lesson._id
              obj.lessonName = _d.lesson[toFound]
              obj.courseId = _d.course._id
              obj.courseName = _d.course[toFound]
              obj.price = _d.lesson.price
          } else if(_d.type =='Course' && _d.course){
            obj.productId = _d.course.product
            obj.lessonId = null
            obj.lessonName = null
            obj.courseId = _d.course._id
            obj.courseName = _d.course[toFound]
            obj.price = _d.course.price
          }

          if(_d.chapter) {

            obj.chapterId = _d.chapter._id
            obj.chapterName = _d.chapter[toFound]
  
          } else {

            obj.chapterId = null
            obj.chapterName = null
  
          }

         

          arr.push(obj)
        });

        res.json(err || arr)
      }
    });
};

findCardsByCourseId = (req, res, courseId) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const lang = req.headers.lang ? req.headers.lang : "en";
    const toFound = lang === "en" ? "name" : "nameAr";
    cardModel.defaultSchema
      .aggregate([
        {
          $match: { courseId: ObjectId(courseId) },
        },
        {
          $group: {
            _id: "$_id",
            name: { $first: `$${toFound}` },
            courseId: { $first: "$courseId" },
          },
        },
      ])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec((err, data) => {
        if (err) res.status(500).send(err);
        else {
          if (data && data.length > 0) resolve(data);
          else resolve([]);
        }
      });
  });
};

create = (req, res) => {
  cardModel.defaultSchema.create(req.body, function (err, result) {
    if (err) res.status(500).send(err);
    else res.status(201).send(result);
  });
};
deleteCard = async (req, res) => {
  let search = {};

  if (req.body._id) {
    search._id = ObjectId(req.body._id);
  }

  if (req.body.studentId) {
    search.studentId = ObjectId(req.body.studentId);
  }

  if (req.body.course) {
    search.course = ObjectId(req.body.course);
  }

  if (req.body.chapter) {
    search.chapter = ObjectId(req.body.chapter);
  }

  if (req.body.lesson) {
    search.lesson = ObjectId(req.body.lesson);
  }

  cardModel.defaultSchema.deleteMany(search, function (err, result) {
    if (err) res.status(500).send(err);
    else res.status(201).send(result);
  });
};
module.exports = {
  deleteCard: deleteCard,
  updateCard: cardModel.genericSchema.update,
  findById: cardModel.genericSchema.findById,
  create,
  findAll,
  findCardsByCourseId,
};
