const courseService = require("./course.service");
const studentService = require("./student.service");
const lessonService = require("./lesson.service");
const notificationModel = require("../models/notification.model");

findLessons = async (req, res) => {
  let courses = await courseService.findAllPass(req, res);

  if (courses && courses.length > 0) {
    courses.forEach((course) => {
      course.chapters.forEach((chapter) => {
        chapter.lessons.forEach((lesson) => {
          let halfHourAgo = new Date(Date.now() + 1000 * 9000);

          // let date = new Date().toLocaleString('en-US', { timeZone: 'Africa/Cairo' });
          // var thirtyMinLater = new Date(new Date().getTime() + 30*60000);
          //     thirtyMinLater.setMinutes(thirtyMinLater.getMinutes() + 30);
          if (new Date(lesson.startDate) < halfHourAgo && !lesson.sendNotific && lesson.liveSession) {
            let notific = {
              users: [
                {
                  userId: course.teacherId,
                  type: "Teacher",
                },
              ],
              notificEn: `Session ${lesson.name} will Start at  ${lesson.startDate}`,
              notificAr: `سيتم بدأ الجلسة ${lesson.nameAr} في  ${lesson.startDate}`,
              reference: {
                type: "sessionStart",
                meetingId: lesson.meetingId,
                meetingPassword: lesson.meetingPassword,
              },
            };

            course.students.forEach((_s) => {
              notific.users.push({
                userId: _s,
                type: "Student",
              });
            });
            lessonService.updateSendNotific(lesson._id, res);
            notificationModel.defaultSchema.create(notific);
          }

          lesson.tasks.forEach((_task) => {
            // let halfHourAgo = new Date(Date.now() - 1000 * 1800);
            // if (new Date(_task.deadline) == halfHourAgo) {
              let newDate = new Date();
              _task.deadline = new Date(_task.deadline)
        if (_task.deadline.getFullYear() == newDate.getFullYear() && _task.deadline.getMonth() == newDate.getMonth() && _task.deadline.getDay() -1 == newDate.getDay() && !_task.sendNotific) {

              let notific = {
                users: [],
                notificEn: `DeadLine delivered Task ${_task.name} will be  ${_task.deadline}`,
                notificAr: `الموعد النهائي لتسليم المهمات ${_task.name} في ${_task.deadline}`,
                reference: {
                  type: "deadLineTask",
                  taskId: _task._id,
                  leassonId: lesson._id,
                  teacherId: course.teacherId,
                  courseId: course._id,
                },
              };

              course.students.forEach((_s) => {
                notific.users.push({
                  userId: _s,
                  type: "Student",
                });
              });
              lessonService.updateSendNotificTask(lesson._id, _task._id);
              notificationModel.defaultSchema.create(notific);
            }
          });
        });
      });
    });
  }
};

deadLineVideoTask = async (req, res) => {

  let students = await studentService.findStudentsLessons(req, res, "pass");

  if (students) {
    students.forEach((_s) => {
      _s.lessons.forEach((lesson) => {
        let newDate = new Date();
        lesson.endDate = new Date(lesson.endDate)

        if (lesson.endDate.getFullYear() == newDate.getFullYear() && lesson.endDate.getMonth() == newDate.getMonth() && lesson.endDate.getDay() -1 == newDate.getDay() && !lesson.sendNotific) {
          let notific = {
            users: [
              {
                userId: _s._id,
                type: "Student",
              },
            ],
            notificEn: `Deadline for watching videos will be at  ${lesson.endDate}`,
            notificAr: `الموعد النهائي لمشاهدة فيديوهات الدرس سيكون في ${lesson.endDate}`,
            reference: {
              type: "deadLineVideo",
              lessonId: lesson.lessonId,
            },
          };
          studentService.updateSendNotific(_s._id,lesson.lessonId);
          notificationModel.defaultSchema.create(notific);
        }
      });
    });
  }
};

setInterval(() => {

  let req1 ={body : {liveSession: true },query : {},headers : {}};
  let res1 = {}
  findLessons(req1,res1);

  let req2 ={body : {endDate: true },query : {},headers : {}};
  let res2 = {}
  deadLineVideoTask(req2,res2);

}, 1000 * 60 * 5);
// }, 1000 * 10);
