const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const courseModel = require("../models/course.model");
const handleFiles = require("../../helpers/handleFiles");
const attachmentPath = require("../../helpers/attachmentPath.json");
const coursePath = attachmentPath.attachments.courseAttachmentPath;
const path = attachmentPath.attachments.courseAttachmentPath;

findAll = (req, res) => {
  let obj = { isDeleted: req.body.isDeleted };
  callCourseAggregateFunc(req, res, obj, "all");
};

findAllPass = (req, res) => {
  let obj = {};

  if (req.body.teacherId) {
    obj.teacherId = ObjectId(req.body.teacherId);
  }

  if (req.body.studentId) {
    obj.students = ObjectId(req.body.studentId);
  }

  if (req.body.courseId) {
    obj._id = ObjectId(req.body.courseId);
  }

  if (req.body.isDeleted) {
    obj.isDeleted = ObjectId(req.body.isDeleted);
  }

  return callCourseAggregateFunc(req, res, obj, "pass");
};

findTeacherCoursesByTeacherId = async (req, res, id) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  courseModel.defaultSchema
    .aggregate([
      {
        $match: { teacherId: ObjectId(id) },
      },
      {
        $lookup: {
          from: "subjects",
          localField: "subjectId",
          foreignField: "_id",
          as: "subject",
        },
      },
      {
        $unwind: {
          path: "$subject",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "grades",
          localField: "gradeId",
          foreignField: "_id",
          as: "grade",
        },
      },
      {
        $lookup: {
          from: "educationsystems",
          localField: "educationSystemId",
          foreignField: "_id",
          as: "educationSystem",
        },
      },
      {
        $unwind: {
          path: "$educationSystem",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: "$grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "chapters",
          localField: "_id",
          foreignField: "courseId",
          as: "chapters",
        },
      },
      {
        $unwind: {
          path: "$chapters",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: "$_id",
          subjectId: { $first: "$subjectId" },
          date: { $first: "$date" },
          subject: { $first: `$subject.${toFound}` },
          gradeId: { $first: "$gradeId" },
          courseName: { $first: `$${toFound}` },
          grade: { $first: `$grade.${toFound}` },
          courseType: { $first: "$courseType" },
          price: { $first: "$price" },
          educationSystemId: { $first: "$educationSystemId" },
          educationSystem: { $first: `$educationSystem.${toFound}` },
          exclusive: { $first: "$exclusive" },
          courseIcon: { $first: "$courseIcon" },
          pricePerLesson: { $first: "$pricePerLesson" },
          courseIntro: { $first: "$courseIntro" },
          product: { $first: "$product" },
          chapters: {
            $push: { _id: "$chapters._id", name: `$chapters.${toFound}` },
          },
          teacherId: { $first: "$teacherId" },
          timeNow: { $first: Date.now() },
        },
      },
      { $sort: { date: -1 } },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => res.json(err || data));
};

findCourseById = (req, res, id, type) => {
  let obj = {
    _id: ObjectId(id),
  };
  return callCourseAggregateFunc(req, res, obj, type);
};

findCourseByLessonIdPass = (req, res, type) => {
  let obj = {};
  return callCourseAggregateFunc(req, res, obj, type);
};

findLiveSessions = (req, res) => {
  let obj = { isDeleted: false };
  return callCourseAggregateFunc(req, res, obj, "all");
};

findStudentCourseById = (req, res, id) => {
  let obj = {
    _id: ObjectId(id),
  };
  return callCourseAggregateFunc(req, res, obj, "student");
};

createWithAttachment = async (req, res) => {
  if (req.body.courseIcon) {
    try {
      req.body.courseIcon = await handleFiles.saveFiles(
        req.body.courseIcon,
        "CourseIcon",
        path
      );
    } catch (error) {
      return res.status(500).send(error);
    }
  }
  courseModel.defaultSchema.create(req.body, function (err, cat) {
    if (err) res.status(500).send(err);
    else res.status(201).send(req.body);
  });
};

callCourseAggregateFunc = (req, res, obj, type) => {
  return new Promise((resolve, reject) => {
    const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 100;
    const lang = req.headers.lang ? req.headers.lang : "en";
    const toFound = lang === "en" ? "name" : "nameAr";

    if (req.body.courseType) {
      obj = { ...obj, courseType: req.body.courseType };
    }
    if (req.body.subjectId) {
      obj = { ...obj, subjectId: ObjectId(req.body.subjectId) };
    }
    if (req.body.gradeId) {
      obj = { ...obj, gradeId: ObjectId(req.body.gradeId) };
    }
    if (req.body.educationSystemId) {
      obj = { ...obj, educationSystemId: ObjectId(req.body.educationSystemId) };
    }
    if (req.body.teacherId) {
      obj = { ...obj, teacherId: ObjectId(req.body.teacherId) };
    }
    if (req.body.status) {
      obj = { ...obj, status: ObjectId(req.body.status) };
    }
    if (req.body.hidden) {
      if(req.body.hidden === 'false'){

        obj = { ...obj, hidden: false }
      } else if(req.body.hidden === 'true'){

        obj = { ...obj, hidden: true }
      }
    }

    let lets = { chapterId: "$chapters._id" };
    let ands = [{ $eq: ["$chapterId", "$$chapterId"] }];

    if (req.body.liveType) {
      lets = { ...lets, liveType: req.body.liveType };
      ands.push({ $eq: ["$liveType", "$$liveType"] });
    }
    if (req.body.liveSession) {
      lets = { ...lets, liveSession: req.body.liveSession };
      ands.push({ $eq: ["$liveSession", "$$liveSession"] });
    }

    if (req.body.active) {
      lets = { ...lets, active: req.body.active };
      ands.push({ $eq: ["$active", "$$active"] });
    }

    // if (req.body.lessonId) {
    //   lets = { ...lets, "lessonId": req.body.lessonId }
    //   ands.push({ $eq: ["$_id", "$$lessonId"] })
    // }

    let matchLesson = {
      $match: {},
    };

    if (req.body.lessonId) {
      matchLesson = {
        $match: { "chapters.lessons._id": ObjectId(req.body.lessonId) },
      };
    }

    let search = { $match: {} };

    if (req.body.search) {
      search = {
        $match: {
          $or: [
            { teacherFirstName: { $regex: req.body.search, $options: "i" } },
            { teacherLastName: { $regex: req.body.search, $options: "i" } },
            { subject: { $regex: req.body.search, $options: "i" } },
            { courseName: { $regex: req.body.search, $options: "i" } },
          ],
        },
      };
    }

    courseModel.defaultSchema
      .aggregate([
        {
          $match: obj,
        },
        {
          $lookup: {
            from: "subjects",
            localField: "subjectId",
            foreignField: "_id",
            as: "subject",
          },
        },
        {
          $unwind: {
            path: "$subject",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "grades",
            localField: "gradeId",
            foreignField: "_id",
            as: "grade",
          },
        },
        {
          $unwind: {
            path: "$grade",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "educationsystems",
            localField: "educationSystemId",
            foreignField: "_id",
            as: "educationSystem",
          },
        },
        {
          $unwind: {
            path: "$educationSystem",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "chapters",
            localField: "_id",
            foreignField: "courseId",
            as: "chapters",
          },
        },
        {
          $unwind: {
            path: "$chapters",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "teachers",
            localField: "teacherId",
            foreignField: "_id",
            as: "teacher",
          },
        },
        {
          $lookup: {
            from: "lessons",
            let: lets,
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: ands,
                  },
                },
              },
            ],
            as: "chapters.lessons",
          },
        },
        {
          $lookup: {
            from: "educationsystems",
            localField: "teacher.educations.educationSystemId",
            foreignField: "_id",
            as: "educationSystemsList",
          },
        },
        {
          $unwind: {
            path: "$teacher",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            subjectId: 1,
            students: 1,
            subject: 1,
            gradeId: 1,
            courseType: 1,
            status: 1,
            name: 1,
            nameAr: 1,
            product: 1,
            price: 1,
            date: 1,
            onSale: 1,
            exclusive: 1,
            educationSystemId: 1,
            courseIcon: 1,
            pricePerLesson: 1,
            courseIntro: 1,
            teacherId: 1,
            chapters: 1,
            grade: 1,
            educationSystem: 1,
            educationSystemsList: 1,
            teacher: 1,
            allLessonsCount: { $size: "$chapters.lessons" },
          },
        },
        {
          $group: {
            _id: "$_id",
            subjectId: { $first: "$subjectId" },
            date: { $first: "$date" },
            onSale: { $first: "$onSale" },
            students: { $first: "$students" },
            subject: { $first: `$subject.${toFound}` },
            gradeId: { $first: "$gradeId" },
            grade: { $first: `$grade.${toFound}` },
            price: { $first: "$price" },
            status: { $first: "$status" },
            courseType: { $first: "$courseType" },
            courseName: { $first: `$${toFound}` },
            educationSystemId: { $first: "$educationSystemId" },
            educationSystem: { $first: `$educationSystem.${toFound}` },
            exclusive: { $first: "$exclusive" },
            courseIcon: { $first: "$courseIcon" },
            pricePerLesson: { $first: "$pricePerLesson" },
            courseIntro: { $first: "$courseIntro" },
            teacherId: { $first: "$teacherId" },
            product: { $first: "$product" },
            teacherFirstName: { $first: "$teacher.firstName" },
            teacherLastName: { $first: "$teacher.lastName" },
            teacherPersonalImage: { $first: "$teacher.personalImage" },
            teacherBio: { $first: "$teacher.bio" },
            certificatesImage: { $first: "$teacher.certificatesImage" },
            teacherCertificates: { $first: "$teacher.certificates" },
            teacherEducations: { $first: "$teacher.educations" },
            educationSystemsList: { $first: "$educationSystemsList" },
            chapters: {
              $push: {
                _id: "$chapters._id",
                name: `$chapters.${toFound}`,
                courseId: "$chapters.courseId",
                lessons: "$chapters.lessons",
              },
            },
            lessonsCount: { $sum: "$allLessonsCount" },
            timeNow: { $first: Date.now() },
          },
        },
        matchLesson,
        search,
        // {
        //   $match: {
        //     'chapters.lessons': { $exists: true, $not: { $size: 0 } }
        //   }
        // },
        { $sort: { date: -1, chapters: 1 } },
      ])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec((err, data) => {
        if (err) {
          res.status(500).send(err);
        } else {
          data.forEach((_c) => {
            _c.isSale = false;
            _c.totalHours = 0;
            if (_c.onSale && _c.onSale.length > 0)
              _c.onSale.forEach((_oS) => {
                if (new Date(_oS.expiryDate) <= new Date()) {
                  _c.isSale = true;
                }
              });
            if (_c.chapters && _c.chapters.length > 0) {
              let lifeArray = [];
              _c.chapters.forEach((_ch) => {
                if (_ch.lessons && _ch.lessons.length > 0) {
                  _ch.lessons.forEach((_l) => {
                    if (_l.liveSession && new Date() < new Date(_l.startDate)) {
                      lifeArray.push({
                        startDate: _l.startDate,
                        maxNumStu: _l.maxNumStu,
                        liveType: _l.liveType,
                      });
                    }
                    _l.items.forEach((_i) => {
                      _c.totalHours += _i.videoLength;
                    });
                  });
                }
              });
              if (lifeArray.length > 0)
                _c.lifeLesson = lifeArray.reduce((a, b) =>
                  a.startDate < b.startDate ? a : b
                );
            }
          });

          if (type === "all" || type === "pass") {
            data.forEach((_d) => {
              _d.teacherEducations = _d.teacherEducations || [];
              _d.educationSystemsList = _d.educationSystemsList || [];

              _d.teacherEducations.forEach((_tE) => {
                _d.educationSystemsList.forEach((_eS) => {
                  if (_eS._id.toString() === _tE.educationSystemId.toString()) {
                    if (toFound === "name") _tE.educationSystem = _eS.name;
                    else _tE.educationSystem = _eS.nameAr;
                  }
                });
              });
              delete _d.educationSystemsList;
            });
          }

          if (type === "student" || type === "pass") {
            resolve(data);
          } else if (type === "oneCourse") {
            res.status(200).send(data[0]);
          } else if (type === "oneCoursePass") {
            resolve(data[0]);
          } else if (req.body.studentId) {
            resolve(data);
          } else if (req.body.teacherId && data && data.length > 0) {
            let obj = {
              teacherId: data[0].teacherId,
              teacherFirstName: data[0].teacherFirstName,
              teacherLastName: data[0].teacherLastName,
              teacherPersonalImage: data[0].teacherPersonalImage,
              teacherBio: data[0].teacherBio,
              certificatesImage: data[0].certificatesImage,
              teacherCertificates: data[0].teacherCertificates,
              teacherEducations: data[0].teacherEducations,
              courses: data,
            };

            res.status(200).send(obj);
          } else {
            res.status(200).send(data);
          }
        }
      });
  });
};

getCourseCoreById = (req, res, courseId) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 50;
  courseModel.defaultSchema
    .findOne({ _id: courseId })
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {
      // let image = handleFiles.readFile(data.courseIcon,coursePath);
      if(err) res.status(500).send(err);
      else {
        data.courseIcon = ''
        res.json(data);

      }
    });
};

// callCourseAnotherAggregateFunc = (req, res, obj) => {
//   const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
//   const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
//   const lang = req.headers.lang ? req.headers.lang : "en";
//   const toFound = lang === "en" ? "name" : "nameAr";
//   if (req.body.subjectId) {
//     obj = { ...obj, subjectId: ObjectId(req.body.subjectId) };
//   }
//   if (req.body.gradeId) {
//     obj = { ...obj, gradeId: ObjectId(req.body.gradeId) };
//   }
//   if (req.body.educationSystemId) {
//     obj = { ...obj, educationSystemId: ObjectId(req.body.educationSystemId) };
//   }

//   if (req.body.courseType) {
//     obj = { ...obj, courseType: req.body.courseType };
//   }

//   let search = { $match: {} };

//   if (req.body.search) {
//     search = {
//       $match: {
//         $or: [
//           { teacherFirstName: { $regex: req.body.search, $options: "i" } },
//           { teacherLastName: { $regex: req.body.search, $options: "i" } },
//           { subjectName: { $regex: req.body.search, $options: "i" } },
//         ],
//       },
//     };
//   }

//   let newObj = { ...obj };
//   courseModel.defaultSchema
//     .aggregate([
//       {
//         $match: newObj,
//       },
//       {
//         $lookup: {
//           from: "subjects",
//           localField: "subjectId",
//           foreignField: "_id",
//           as: "subject",
//         },
//       },
//       {
//         $unwind: {
//           path: "$subject",
//           preserveNullAndEmptyArrays: true,
//         },
//       },
//       {
//         $lookup: {
//           from: "teachers",
//           localField: "teacherId",
//           foreignField: "_id",
//           as: "teacher",
//         },
//       },
//       {
//         $unwind: {
//           path: "$teacher",
//           preserveNullAndEmptyArrays: true,
//         },
//       },
//       {
//         $project: {
//           subjectId: 1,
//           subject: 1,
//           name: 1,
//           nameAr: 1,
//           price: 1,
//           courseType: 1,
//           date: 1,
//           exclusive: 1,
//           courseIcon: 1,
//           pricePerLesson: 1,
//           courseIntro: 1,
//           teacherId: 1,
//           teacher: 1,
//         },
//       },
//       {
//         $group: {
//           _id: "$_id",
//           courseName: { $first: `$${toFound}` },
//           subjectId: { $first: "$subjectId" },
//           date: { $first: "$date" },
//           subjectName: { $first: `$subject.${toFound}` },
//           price: { $first: "$price" },
//           courseType: { $first: "$courseType" },
//           exclusive: { $first: "$exclusive" },
//           courseIcon: { $first: "$courseIcon" },
//           pricePerLesson: { $first: "$pricePerLesson" },
//           courseIntro: { $first: "$courseIntro" },
//           teacherId: { $first: "$teacherId" },
//           teacherFirstName: { $first: "$teacher.firstName" },
//           teacherLastName: { $first: "$teacher.lastName" },
//           teacherPersonalImage: { $first: "$teacher.personalImage" },
//           timeNow: { $first: Date.now() },
//         },
//       },
//       search,
//       { $sort: { date: -1 } },
//     ])
//     .skip((pageNumber - 1) * pageSize)
//     .limit(pageSize)
//     .exec((err, data) => {
//       res.json(err || data);
//     });
// };

// callCourseAnotherAggregateFunc = (req, res, obj) => {
//   const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
//   const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
//   const lang = req.headers.lang ? req.headers.lang : "en";
//   const toFound = lang === "en" ? "name" : "nameAr";
//   if (req.body.subjectId) {
//     req.body.subjectId = ObjectId(req.body.subjectId);
//   }
//   if (req.body.gradeId) {
//     req.body.gradeId = ObjectId(req.body.gradeId);
//   }
//   if (req.body.educationSystemId) {
//     req.body.educationSystemId = ObjectId(req.body.educationSystemId);
//   }
//   let newObj = { ...obj, ...req.body };
//   courseModel.defaultSchema
//     .aggregate([
//       {
//         $match: newObj,
//       },
//       {
//         $lookup: {
//           from: "subjects",
//           localField: "subjectId",
//           foreignField: "_id",
//           as: "subject",
//         },
//       },
//       {
//         $unwind: {
//           path: "$subject",
//           preserveNullAndEmptyArrays: true,
//         },
//       },
//       {
//         $lookup: {
//           from: "teachers",
//           localField: "teacherId",
//           foreignField: "_id",
//           as: "teacher",
//         },
//       },
//       {
//         $unwind: {
//           path: "$teacher",
//           preserveNullAndEmptyArrays: true,
//         },
//       },
//       {
//         $project: {
//           subjectId: 1,
//           subject: 1,
//           price: 1,
//           courseType: 1,
//           date: 1,
//           exclusive: 1,
//           courseIcon: 1,
//           courseIntro: 1,
//           teacherId: 1,
//           teacher: 1,
//         },
//       },
//       {
//         $group: {
//           _id: "$_id",
//           subjectId: { $first: "$subjectId" },
//           date: { $first: "$date" },
//           subjectName: { $first: `$subject.${toFound}` },
//           price: { $first: "$price" },
//           courseType: { $first: "$courseType" },
//           exclusive: { $first: "$exclusive" },
//           courseIcon: { $first: "$courseIcon" },
//           courseIntro: { $first: "$courseIntro" },
//           teacherId: { $first: "$teacherId" },
//           teacherFirstName: { $first: "$teacher.firstName" },
//           teacherLastName: { $first: "$teacher.lastName" },
//           teacherPersonalImage: { $first: "$teacher.personalImage" },
//           timeNow: { $first: Date.now() },
//         },
//       },
//       { $sort: { date: -1 } },
//     ])
//     .skip((pageNumber - 1) * pageSize)
//     .limit(pageSize)
//     .exec((err, data) => res.json(err || data));
// };

getNewReleasesCourses = async (req, res) => {
  let obj = { isDeleted: false };
  callCourseAggregateFunc(req, res, obj);
};

getExclusiveCourses = async (req, res, id) => {
  let obj = { isDeleted: false, exclusive: true };
  callCourseAggregateFunc(req, res, obj);
};

getStudentCourses = async (req, res) => {
  if (req.body.courses && req.body.courses.length > 0) {
    let result = req.body.courses.map((course) => ObjectId(course));
    let obj = {
      _id: { $in: result },
      isDeleted: false,
    };

    if (req.body.courseType) {
      obj = { ...obj, courseType: req.body.courseType };
    }
    if (req.body.subjectId) {
      obj = { ...obj, subjectId: ObjectId(req.body.subjectId) };
    }
    if (req.body.gradeId) {
      obj = { ...obj, gradeId: ObjectId(req.body.gradeId) };
    }
    if (req.body.educationSystemId) {
      obj = {
        ...obj,
        educationSystemId: ObjectId(req.body.educationSystemId),
      };
    }
    findCourseBasicInfo(req, res, obj);
  }
};

findCourseBasicInfo = async (req, res, obj) => {
  const pageNumber = req.query.pageNumber ? req.query.pageNumber : 1;
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  courseModel.defaultSchema
    .aggregate([
      {
        $match: obj,
      },
      {
        $lookup: {
          from: "subjects",
          localField: "subjectId",
          foreignField: "_id",
          as: "subject",
        },
      },
      {
        $unwind: {
          path: "$subject",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "chapters",
          localField: "_id",
          foreignField: "courseId",
          as: "chapters",
        },
      },
      {
        $unwind: {
          path: "$chapters",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "lessons",
          localField: "chapters._id",
          foreignField: "chapterId",
          as: "chapters.lessons",
        },
      },
      {
        $lookup: {
          from: "teachers",
          localField: "teacherId",
          foreignField: "_id",
          as: "teacher",
        },
      },
      {
        $unwind: {
          path: "$teacher",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          subjectId: 1,
          subject: 1,
          gradeId: 1,
          courseType: 1,
          onSale: 1,
          status: 1,
          name: 1,
          nameAr: 1,
          price: 1,
          date: 1,
          exclusive: 1,
          educationSystemId: 1,
          courseIcon: 1,
          product: 1,
          pricePerLesson: 1,
          courseIntro: 1,
          teacherId: 1,
          chapters: 1,
          teacher: 1,
          allLessonsCount: { $size: "$chapters.lessons" },
        },
      },
      {
        $group: {
          _id: "$_id",
          subjectId: { $first: "$subjectId" },
          subject: { $first: `$subject.${toFound}` },
          courseName: { $first: `$${toFound}` },
          date: { $first: "$date" },
          gradeId: { $first: "$gradeId" },
          price: { $first: "$price" },
          courseType: { $first: "$courseType" },
          status: { $first: "$status" },
          onSale: { $first: "$onSale" },
          educationSystemId: { $first: "$educationSystemId" },
          exclusive: { $first: "$exclusive" },
          courseIcon: { $first: "$courseIcon" },
          product: { $first: "$product" },
          pricePerLesson: { $first: "$pricePerLesson" },
          courseIntro: { $first: "$courseIntro" },
          teacherId: { $first: "$teacherId" },
          teacherFirstName: { $first: "$teacher.firstName" },
          teacherLastName: { $first: "$teacher.lastName" },
          teacherPersonalImage: { $first: "$teacher.personalImage" },
          lessonsCount: { $sum: "$allLessonsCount" },
          watchedLessons: { $sum: 0 },
          timeNow: { $first: Date.now() },
        },
      },
      { $sort: { date: -1 } },
    ])
    .skip((pageNumber - 1) * pageSize)
    .limit(pageSize)
    .exec((err, data) => {
      if (err) res.status(500).send(err);
      else {
        if (req.body.lessons && req.body.lessons.length > 0) {
          for (const lesson of req.body.lessons) {
            for (const d of data) {
              if (lesson.seen && d._id.equals(lesson.courseId)) {
                d.watchedLessons++;
              }
            }
          }
        }
        res.status(200).send(data);
      }
    });
};

updateFullCourse = async (req, res,id) => {

  try {

  let course = await courseModel.defaultSchema.findOne({ _id : ObjectId(id)});

  if(course) {

      if(req.body.courseIcon){

        await handleFiles.deleteFile(course.courseIcon, path);

        let courseIcon = await handleFiles.saveFiles(
          req.body.courseIcon,
          "CourseIcon",
          path 
          );
          req.body.courseIcon = courseIcon;
        } else {
          req.body.courseIcon = course.courseIcon;
        }
        courseModel.defaultSchema.findOneAndUpdate(
          { _id : ObjectId(id) },
          req.body,
          function (err, data) {
          if (err) res.status(500).send(err);
          else res.status(200).send(data);
      }
    );
  }

} catch (error) {
  return res.status(500).send(error);
}

};

filterCourse = (req, res) => {
  if (req.body.subjectId) {
    req.body.subjectId = ObjectId(req.body.subjectId);
  }
  if (req.body.gradeId) {
    req.body.gradeId = ObjectId(req.body.gradeId);
  }
  if (req.body.educationSystemId) {
    req.body.educationSystemId = ObjectId(req.body.educationSystemId);
  }
  callCourseAggregateFunc(req, res, req.body, "default");
};

addFavoriteIntoCourse = async (req, res, id) => {
  courseModel.defaultSchema.findByIdAndUpdate(
    req.body.courseId,
    {
      $addToSet: {
        favorites: id,
      },
    },
    {
      // While Update: show last updated document with new values
      new: true,
      // While Update: the default values will inserted without passing values explicitly
      setDefaultsOnInsert: true,
    },
    function (err, data) {
      if (err) res.status(500).send(err);
      // else if (data === null) res.status(404).send("ID is not found");
      // else res.status(200).send(data);
    }
  );
};
removeCourseFavorite = async (req, res, id, courseId) => {
  courseModel.defaultSchema.findByIdAndUpdate(
    courseId,
    { $pull: { favorites: id } },
    { safe: true, multi: true },
    function (err, data) {
      if (err) res.status(500).send(err);
      // else if (data === null) res.status(404).send("ID is not found");
      // else res.sendStatus(200);
    }
  );
};
removeCoursePackage = async (req, res, id, courseId) => {
  courseModel.defaultSchema.findByIdAndUpdate(
    courseId,
    { $pull: { onSale: { packageId: ObjectId(id) } } },
    { safe: true, multi: true },
    function (err, data) {
      if (err) res.status(500).send(err);
      // else if (data === null) res.status(404).send("ID is not found");
      // else res.sendStatus(200);
    }
  );
};
addStudentIntoCourse = async (courseIds, studentId) => {
  return new Promise((resolve, reject) => {
    courseModel.defaultSchema.updateMany(
      {
        _id: courseIds,
      },
      {
        $addToSet: {
          students: studentId,
        },
      },
      {
        // While Update: show last updated document with new values
        new: true,
        // While Update: the default values will inserted without passing values explicitly
        setDefaultsOnInsert: true,
      },
      function (err, data) {
        if (err) reject(data);
        else resolve(data);
      }
    );
  });
};

onSaleCourse = async (res, id, result) => {
  courseModel.defaultSchema.findByIdAndUpdate(
    id,
    {
      $push: {
        onSale: { packageId: result._id, expiryDate: result.expiryDate },
      },
    },
    { safe: true, upsert: true },
    // {
    //   // While Update: show last updated document with new values
    //   new: true,
    //   // While Update: the default values will inserted without passing values explicitly
    //   setDefaultsOnInsert: true,
    // },
    function (err, data) {
      if (err) res.status(500).send(err);
      // else if (data === null) res.status(404).send("ID is not found");
      // else res.status(200).send(data);
    }
  );
};

module.exports = {
  deleteCourse: courseModel.genericSchema.delete,
  updateCourse: courseModel.genericSchema.update,
  findById: findCourseById,
  create: createWithAttachment,
  findAll,
  findAllPass,
  onSaleCourse,
  findCourseByLessonIdPass,
  addFavoriteIntoCourse,
  findLiveSessions,
  findTeacherCoursesByTeacherId,
  removeCourseFavorite,
  removeCoursePackage,
  getNewReleasesCourses,
  getStudentCourses,
  getExclusiveCourses,
  filterCourse,
  findStudentCourseById,
  addStudentIntoCourse,
  getCourseCoreById,
  updateFullCourse,
};
