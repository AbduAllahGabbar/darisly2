const express = require("express");
const cardRouter = express.Router();
const cardController = require("../controllers/card.controller");
const roles = require("../../helpers/roles");

cardRouter.get(
  "/",
  cardController.getAllData
);
cardRouter.post("/",  cardController.create);
cardRouter.get(
  "/:id",
  cardController.findById
);
cardRouter.put(
  "/:id",
  
  cardController.updateCard
);
cardRouter.delete(
  "/",
  
  cardController.deleteCard
);

module.exports = cardRouter;