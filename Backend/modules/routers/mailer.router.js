const express = require("express");
const mailerRouter = express.Router();
const mailerController = require("../controllers/mailer.controller");
const roles = require("../../helpers/roles");

mailerRouter.post("/", mailerController.create);
mailerRouter.get("/", mailerController.findMailer);


module.exports = mailerRouter;
