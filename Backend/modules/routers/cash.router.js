const express = require("express");
const cashRouter = express.Router();
const cashController = require("../controllers/cash.controller");

cashRouter.post(
  "/key",
  cashController.generatePaymentKeyWithOrderRegistration
);

cashRouter.post(
  "/notification-callback",
  cashController.notificationCallback
);

cashRouter.get("/response-callback", cashController.responseCallback);

module.exports = cashRouter;