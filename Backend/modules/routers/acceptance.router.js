const express = require("express");
const acceptanceRouter = express.Router();
const acceptanceController = require("../controllers/acceptance.controller");

acceptanceRouter.post(
  "/key",
  acceptanceController.generatePaymentKeyWithOrderRegistration
);

acceptanceRouter.post(
  "/notification-callback",
  acceptanceController.notificationCallback
);

acceptanceRouter.get("/response-callback", acceptanceController.responseCallback);

module.exports = acceptanceRouter;
