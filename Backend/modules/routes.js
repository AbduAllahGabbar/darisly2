const express = require("express");
const router = express.Router();

const adminRouter = require("./routers/admin.router");
const promoCodeRouter = require("./routers/promoCode.router");
const giftCardRouter = require("./routers/giftCard.router");
const communityRouter = require("./routers/community.router");
const studentRouter = require("./routers/student.router");
const teacherRouter = require("./routers/teacher.router");
const chapterRouter = require("./routers/chapter.router");
const cardRouter = require("./routers/card.router");
const lessonRouter = require("./routers/lesson.router");
const deliveredTaskRouter = require("./routers/task.router");
const quizRouter = require("./routers/quiz.router");
const packageRouter = require("./routers/package.router");
const codeRouter = require("./routers/code.router");
const notificationRouter = require("./routers/notification.router");


router.use("/admin", adminRouter);
router.use("/promoCode", promoCodeRouter);
router.use("/giftCard", giftCardRouter);
router.use("/community", communityRouter);
router.use("/student", studentRouter);
router.use("/teacher", teacherRouter);
router.use("/chapter", chapterRouter);
router.use("/card", cardRouter);
router.use("/lesson", lessonRouter);
router.use("/task", deliveredTaskRouter);
router.use("/quiz", quizRouter);
router.use("/package", packageRouter);
router.use("/code", codeRouter);
router.use("/notification", notificationRouter);

module.exports = router;
