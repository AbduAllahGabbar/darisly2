const studentService = require("../services/student.service");
const bcrypt = require("bcryptjs");
const logger = require("../../helpers/logging");
const courseController = require("./course.controller");
const taskController = require("./task.controller");
const quizController = require("./quiz.controller");
const chapterController = require("./chapter.controller");
const mailerController = require("./mailer.controller");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
getAllData = (req, res) => {
  try {
    studentService.findAll(req, res);
  } catch (error) {
    logger.error(error);
  }
};

createStudent = (req, res, next) => {
  try {
    if (req.body.password.length < 6) {
      return res.status(403).send("Password must be at least 6 chars");
    }
    if (req.body.password != req.body.confirmPassword) {
      return res.status(403).send("Password is not equal to confirm password");
    }
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return callback(err);
      }
      bcrypt.hash(req.body.password, salt, (err, hash) => {
        if (err) {
          return next(err);
        }
        req.body.password = hash;
        delete req.body.confirmPassword;
        studentService.create(req, res, (result) => {
          if (result && result._id)
            mailerController.createUser(result, "Student");
        });
      });
    });
  } catch (error) {
    logger.error(error);
  }
};
socialMediaRegister = (req, res) => {
  try {
    studentService.create(req, res,result =>{

    });
  } catch (error) {
    logger.error(error);
  }
};
findById = (req, res) => {
  try {
    const id = req.params.id;
    studentService.findById(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};

verifyStudent = async (req, res) => {
  try {
    let body = {
      email: req.body.email,
      userType: "Student",
      type: "Register",
      code: req.body.code,
    };
    let result = await mailerController.checkMailer(body, res);
    if (result && result._id) {
      studentService.verifyStudent(req, res);
    } else {
      res.status(500).send("Code Is Error");
    }
  } catch (error) {
    logger.error(error);
  }
};

updateStudent = (req, res) => {
  try {
    const id = req.params.id;
    if (req.body.email) {
      return res.status(403).send("Email Cannot be updated");
    }
    studentService.updateStudent(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
deleteStudent = (req, res) => {
  try {
    const id = req.params.id;
    studentService.deleteStudent(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
identifyStudent = (req, res) => {
  try {
    studentService.findStudentAccount(req, res);
  } catch (error) {
    logger.error(error);
  }
};
socialMediaLogin = (req, res) => {
  try {
    studentService.socialMediaLogin(req, res);
  } catch (error) {
    logger.error(error);
  }
};
logout = (req, res) => {
  student = {};
  token = null;
};
addPurchaseIntoStudent = async (req, res) => {
  try {
    const id = req.params.id;
    try {
      let courseIds = [];
      req.body.lessons.forEach((_l) => {
        courseIds.push(_l.courseId);
      });

      function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
      }

      let courseIdsUnique = courseIds.filter(onlyUnique);
      let result = await courseController.addStudentIntoCourse(
        courseIdsUnique,
        id
      );
      if (result)
        studentService.addPurchaseIntoStudent(
          req,
          res,
          courseIdsUnique,
          id,
          "default"
        );
    } catch (error) {
      res.send(error);
    }
  } catch (error) {
    logger.error(error);
  }
};

addPurchaseIntoStudentByCode = async (req, res) => {
  try {
    const id = req.body.studentId;
    try {
      let result = await courseController.addStudentIntoCourse(
        req.body.courseId,
        id
      );

      if (result) {
        return studentService.addPurchaseIntoStudent(
          req,
          res,
          [req.body.courseId],
          id,
          "other"
        );
      }
    } catch (error) {
      res.send(error);
    }
  } catch (error) {
    logger.error(error);
  }
};
findStudentCourses = async (req, res) => {
  try {
    const id = req.params.id;
    let result = await studentService.findStudentCourses(req, res, id);

    if (result) {
      req.body.courses = result[0].courses;
      req.body.lessons = result[0].lessons;
      courseController.getStudentCourses(req, res);
    }
  } catch (error) {
    logger.error(error);
  }
};
findStudentLessons = (req, res) => {
  try {
    const id = req.params.id;
    studentService.findStudentLessons(req, res, id, "default");
  } catch (error) {
    logger.error(error);
  }
};
addFavoriteIntoStudent = (req, res) => {
  try {
    const id = req.params.id;
    courseController.addFavoriteIntoCourse(req, res, id);
    studentService.addFavoriteIntoStudent(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
removeStudentFavorite = (req, res) => {
  try {
    const id = req.params.id;
    const courseId = req.params.courseId;
    courseController.removeCoursePackage(req, res, id, courseId);
    studentService.removeStudentFavorite(req, res, id, courseId);
  } catch (error) {
    logger.error(error);
  }
};
findStudentFavorites = async (req, res) => {
  try {
    const id = req.params.id;
    let result = await studentService.findStudentFavorites(req, res, id);
    if (result) {
      req.body.courses = result;
      courseController.getStudentCourses(req, res);
    }
  } catch (error) {
    logger.error(error);
  }
};
checkStudentCourseInFavorite = (req, res) => {
  try {
    const studentId = req.params.studentId;
    const courseId = req.params.courseId;
    studentService.checkStudentCourseInFavorite(req, res, studentId, courseId);
  } catch (error) {
    logger.error(error);
  }
};

forgetPassword = async (req, res) => {
  try {
    let body = {
      email: req.body.email,
      userType: "Student",
      type: "Forget",
      code: req.body.code,
    };
    let result = await mailerController.checkMailer(body, res);
    if (result) studentService.forgetPassword(req, res);
    else res.status(500).send("Code Is Error");
  } catch (error) {
    logger.error(error);
  }
};

changePassword = (req, res) => {
  try {
    const id = req.params.id;
    studentService.changePassword(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
updateStudentLessonPurchase = (req, res) => {
  try {
    return studentService.updateStudentLessonPurchase(req, res);
  } catch (error) {
    logger.error(error);
  }
};

findStudentCourseByCourseId = async (req, res) => {
  try {
    const studentId = req.params.studentId;
    const courseId = req.params.courseId;

    let studentLessons = await studentService.findStudentLessons(
      req,
      res,
      studentId,
      "other"
    );

    let deliveredQuizzes = await quizController.findQuizByStudentId(
      req,
      res,
      studentId
    );

    let deliveredTasks = await taskController.findTaskByStudentId(
      req,
      res,
      studentId
    );

    let course = await courseController.findStudentCourseById(
      req,
      res,
      courseId
    );

    if (studentLessons.lessons && studentLessons.lessons.length > 0) {
      studentLessons.lessons.forEach((_sL) => {
        _sL.deliveredTasks = _sL.deliveredTasks || [];
        _sL.deliveredQuizzes = _sL.deliveredQuizzes || [];

        if (deliveredTasks && deliveredTasks.length > 0) {
          deliveredTasks.forEach((_t) => {
            if (_sL.lessonId && _t.lessonId) {
              if (_sL.lessonId.toString() === _t.lessonId.toString()) {
                _sL.deliveredTasks.push(_t);
              }
            }
          });
        }

        if (deliveredQuizzes && deliveredQuizzes.length > 0) {
          deliveredQuizzes.forEach((_q) => {
            if (_sL.lessonId && _q.lessonId) {
              if (_sL.lessonId.toString() === _q.lessonId.toString()) {
                _sL.deliveredQuizzes.push(_q);
              }
            }
          });
        }
      });
    }

    let obj = { course: course[0], studentLessons: studentLessons };

    res.send(obj);
  } catch (error) {
    logger.error(error);
  }
};
findStudentTeacherByCourseId = async (req, res) => {
  try {
    const studentId = req.params.studentId;
    const courseId = req.params.courseId;

    let studentLessons = await studentService.findStudentLessons(
      req,
      res,
      studentId,
      "other"
    );

    let deliveredQuizzes = await quizController.findQuizByStudentId(
      req,
      res,
      studentId
    );

    let deliveredTasks = await taskController.findTaskByStudentId(
      req,
      res,
      studentId
    );

    let chapters = await chapterController.findChaptersByCourseId(
      req,
      res,
      courseId
    );

    if (studentLessons.lessons && studentLessons.lessons.length > 0) {
      studentLessons.lessons.forEach((_sL) => {
        _sL.deliveredTasks = _sL.deliveredTasks || [];
        _sL.deliveredQuizzes = _sL.deliveredQuizzes || [];

        if (deliveredTasks && deliveredTasks.length > 0) {
          deliveredTasks.forEach((_t) => {
            if (_sL.lessonId && _t.lessonId) {
              if (_sL.lessonId.toString() === _t.lessonId.toString()) {
                _sL.deliveredTasks.push(_t);
              }
            }
          });
        }

        if (deliveredQuizzes && deliveredQuizzes.length > 0) {
          deliveredQuizzes.forEach((_q) => {
            if (_sL.lessonId && _q.lessonId) {
              if (_sL.lessonId.toString() === _q.lessonId.toString()) {
                _sL.deliveredQuizzes.push(_q);
              }
            }
          });
        }
        chapters.forEach((_ch) => {
          if (_ch._id.toString() === _sL.chapterId.toString()) {
            _ch.lessons = _ch.lessons || [];
            _ch.lessons.push(_sL);
          }
        });
      });
    }
    let obj = { chapters: chapters };

    res.send(obj);
  } catch (error) {
    logger.error(error);
  }
};
findStudentById = async (id) => {
  try {
    return await studentService.findStudentById(id);
  } catch (error) {
    logger.error(error);
  }
};
findMyOrders = async (req, res) => {
  try {
    let courses = await courseController.getAllDataPass(req, res);
    return await studentService.findMyOrders(req, res, courses);
  } catch (error) {
    logger.error(error);
  }
};

findMyOrdersSearch = async (req, res) => {
  try {
    if(req.params.filter == 'null'){
      req.body.filter = ''
    } else {

      req.body =req.params
    }
    let courses = await courseController.getAllDataPass(req, res);
    return await studentService.findMyOrdersAdmin(req, res, courses);
  } catch (error) {
    logger.error(error);
  }
};

findStudentsForTeacherAnalytics = async (req, res) => {
  try {
    let course = await courseController.findByIdPass(req, res);
    let students = await studentService.findStudentsLessons(req, res, "pass");
    if (students && course) {
      let result = {
        revenues: 0,
        studentsCount: students.length,
        ordersCount: 0,
        commits: 0,
      };
      let courseLessonsId = [];

      let checker = (arr, target) => target.every((v) => arr.includes(v));

      course.chapters = course.chapters || [];
      course.chapters.forEach((_ch) => {
        _ch.lessons = _ch.lessons || [];
        _ch.lessons.forEach((_l) => {
          if (_l._id && _l.active) courseLessonsId.push(_l._id.toString());
        });
      });

      students.forEach((_s) => {
        let studentLessonId = [];

        _s.lessons.forEach((_sL) => {
          if (_sL.courseId.toString() && course._id.toString()) {
            studentLessonId.push(_sL.lessonId.toString());
          }
        });

        _s.myOrders.forEach((_myO) => {
          _myO.date.setHours(0,0,0,0)
          if (
            req.body.allDate ||
            (new Date(_myO.date) >= new Date(req.body.dateFrom) &&
              new Date(_myO.date) <= new Date(req.body.dateTo))
          ) {
            let revenues = 0;
            result.ordersCount = result.ordersCount + 1;
            _myO.packages.forEach((_p) => {
              revenues += _p.newPrice || 0;
            });

            _s.lessons.forEach((_sL) => {
              let order = _sL.order.find((_o) => {
                return _o.code === _myO.code;
              });
              if (
                order &&
                order.code &&
                !_myO.packages.some((_p) => _p.courseId === _sL.courseId)
              ) {
                revenues += order.price || 0;
              }
            });

            if (
              _myO.promoCode &&
              _myO.promoCode.promoCode &&
              _myO.promoCode.value
            ) {
              if (_myO.promoCode.discount === "byAmount") {
                revenues = revenues - _myO.promoCode.value;
              } else if (_myO.promoCode.discount === "byPercentage") {
                revenues = revenues - (revenues * _myO.promoCode.value) / 100;
              }
            } else {
              result.revenues += revenues;
            }
          }
        });

        if (checker(studentLessonId, courseLessonsId)) {
          result.commits = result.commits + 1;
        }
      });

      res.send(result);
    } else res.status(500).send("students Pay Not Found");
  } catch (error) {
    logger.error(error);
  }
};

findMyStudentsForTeacher = async (req, res) => {
  try {
    let course = await courseController.findByIdPass(req, res);
    let students = await studentService.findStudentsLessons(req, res, "pass");

    if (students && course) {
      let result = { course: { ...course }, studentsList: [] };

      students.forEach((_s) => {
        let lessonsDelivered = 0;

        let student = {
          _id: _s._id,
          score: 0,
          firstName: _s.firstName,
          lastName: _s.lastName,
          phoneNumber: _s.phoneNumber,
          lessons: [],
        };

        _s.lessons.forEach((_sL) => {
          if (_sL.courseId.toString() && course._id.toString()) {
            let lesson = {
              lessonId: _sL.lessonId,
              deliveredTasks: _sL.deliveredTasks,
              deliveredQuizzes: _sL.deliveredQuizzes,
              score: 0,
            };

            for (let i = _sL.deliveredTasks.length; i--; ) {
              lesson.score += _sL.deliveredTasks[i].score || 0;
            }

            for (let i = _sL.deliveredQuizzes.length; i--; ) {
              lesson.score += _sL.deliveredQuizzes[i].score || 0;
            }
            let length =
              (_sL.deliveredTasks.length || 0) +
              (_sL.deliveredQuizzes.length || 0) * 100;
            if (length) {
              lesson.score = (lesson.score / length) * 100;
            }
            if (
              _sL.deliveredTasks.length > 0 ||
              _sL.deliveredQuizzes.length > 0
            ) {
              lessonsDelivered += 1;
            }

            student.lessons.push(lesson);
          }
        });

        let lessons_score = 0;
        for (let i = student.lessons.length; i--; ) {
          lessons_score += student.lessons[i].score || 0;
        }
        if (lessonsDelivered)
          student.score = (lessons_score / (lessonsDelivered * 100)) * 100;
        result.studentsList.push(student);
      });

      res.send(result);
    } else res.status(500).send("students Not Found");
  } catch (error) {
    logger.error(error);
  }
};

findPromoIntoStudent = (req, res) => {
  try {
    return studentService.findPromoIntoStudent(req, res);
  } catch (error) {
    logger.error(error);
  }
};
module.exports = {
  getAllData,
  createStudent,
  findById,
  updateStudent,
  deleteStudent,
  identifyStudent,
  logout,
  addPurchaseIntoStudent,
  findStudentsForTeacherAnalytics,
  findMyStudentsForTeacher,
  findStudentCourses,
  findStudentLessons,
  addFavoriteIntoStudent,
  findStudentFavorites,
  verifyStudent,
  forgetPassword,
  changePassword,
  updateStudentLessonPurchase,
  findStudentCourseByCourseId,
  findStudentTeacherByCourseId,
  checkStudentCourseInFavorite,
  findStudentById,
  findMyOrders,
  findMyOrdersSearch,
  addPurchaseIntoStudentByCode,
  removeStudentFavorite,
  socialMediaRegister,
  socialMediaLogin,
  findPromoIntoStudent,
};
