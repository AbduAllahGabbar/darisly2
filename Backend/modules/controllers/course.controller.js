const Joi = require("@hapi/joi");
const courseValidator = require("../validators/course.validator");
const courseService = require("../services/course.service");
const studentService = require("../services/student.service");
const notificationController = require("./notification.controller");
const logger = require("../../helpers/logging");

getAllData = (req, res) => {
  try {
    req.body.isDeleted = false;
    courseService.findAll(req, res);
  } catch (error) {
    logger.error(error);
  }
};

getAllDataPass = (req, res) => {
  try {
    return courseService.findAllPass(req, res);
  } catch (error) {
    logger.error(error);
  }
};

getAllDeletedCourses = (req, res) => {
  try {
    req.body.isDeleted = true;
    courseService.findAll(req, res);
  } catch (error) {
    logger.error(error);
  }
};
create = (req, res) => {
  try {
    // Validation

    if (req.body.courseType === "Educations" && !req.body.gradeId) {
      return res
        .status(422)
        .send("Must Select gradeId when course type is educations");
    }
    if (req.body.courseType === "Skills" && req.body.gradeId) {
      delete req.body.gradeId;
    }
    Joi.validate(req.body, courseValidator.isValidCourse, (err, body) => {
      try {
        if (err) return res.status(422).send(err.details[0]);
        courseService.create(req, res);
      } catch (error) {
        console.log(error);
        logger.error(error);
      }
    });
  } catch (error) {
    logger.error(error);
  }
};

findById = (req, res) => {
  try {
    const id = req.params.id;
    courseService.findById(req, res, id, "oneCourse");
  } catch (error) {
    logger.error(error);
  }
};
findByIdPass = (req, res) => {
  try {
    const id = req.body.courseId;
    return courseService.findById(req, res, id, "oneCoursePass");
  } catch (error) {
    logger.error(error);
  }
};
findCourseByLessonIdPass = async (req, res) => {
  try {
    let course = await courseService.findCourseByLessonIdPass(
      req,
      res,
      "oneCoursePass"
    );

    if (course) {
      notificationController.newSessionEnrolledCourse(req, course);
    }
    // return courseService.findCourseByLessonIdPass(req, res, "oneCoursePass");
  } catch (error) {
    logger.error(error);
  }
};
updateCourse = (req, res) => {
  try {
    const id = req.params.id;

    courseService.updateCourse(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
addFavoriteIntoCourse = (req, res, id) => {
  try {
    courseService.addFavoriteIntoCourse(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
removeCourseFavorite = (req, res, id, courseId) => {
  try {
    courseService.removeCourseFavorite(req, res, id, courseId);
  } catch (error) {
    logger.error(error);
  }
};
removeCoursePackage = (req, res, id, courseId) => {
  try {
    courseService.removeCoursePackage(req, res, id, courseId);
  } catch (error) {
    logger.error(error);
  }
};
onSaleCourse = (req, res, result) => {
  try {
    const id = req.body.courseId;

    courseService.onSaleCourse(res, id, result);
  } catch (error) {
    logger.error(error);
  }
};
deleteCourse = (req, res) => {
  try {
    const id = req.params.id;
    courseService.deleteCourse(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
findTeacherCoursesByTeacherId = (req, res) => {
  try {
    const id = req.params.id;
    courseService.findTeacherCoursesByTeacherId(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
getNewReleasesCourses = (req, res) => {
  try {
    courseService.getNewReleasesCourses(req, res);
  } catch (error) {
    logger.error(error);
  }
};
getStudentCourses = (req, res) => {
  try {
    courseService.getStudentCourses(req, res);
  } catch (error) {
    logger.error(error);
  }
};
getCourseCoreById = (req, res) => {
  try {
    const id = req.params.id;
    courseService.getCourseCoreById(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
getExclusiveCourses = (req, res) => {
  try {
    courseService.getExclusiveCourses(req, res);
  } catch (error) {
    logger.error(error);
  }
};
filterCourse = (req, res) => {
  try {
    courseService.filterCourse(req, res);
  } catch (error) {
    logger.error(error);
  }
};
findStudentCourseById = (req, res, id) => {
  try {
    return courseService.findStudentCourseById(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};

getCourseCalender = async (req, res) => {
  try {
    const lang = req.headers.lang ? req.headers.lang : "en";
    const toFound = lang === "en" ? "name" : "nameAr";

    req.body.active = true;

    let courses = await courseService.findAllPass(req, res);
    if (courses) {
      let result = { lives: [], deadlines: [] };

      courses.forEach((_c) => {
        _c.chapters.forEach((_ch) => {
          _ch.lessons.forEach((_l) => {
            if (_l.liveSession) {
              result.lives.push({
                courseId: _c._id,
                courseName: _c.courseName,
                chapterName: _ch.chapterName,
                lessonId: _l._id,
                lessonName: _l[toFound],
                dateTime: _l.startDate,
                meetingId: _l.meetingId,
                meetingPassword: _l.meetingPassword,
              });
            }

            _l.tasks.forEach((_t) => {
              result.deadlines.push({
                courseId: _c._id,
                teacherId: _c.teacherId,
                courseName: _c.courseName,
                chapterName: _ch.chapterName,
                lessonId: _l._id,
                lessonName: _l[toFound],
                dateTime: _l.startDate,
                task: _t,
              });
            });
          });
        });
      });

      return res.send(result);
    } else {
      return res.status(500).send("Not Found Any Sessions");
    }
  } catch (error) {
    logger.error(error);
  }
};

updateFullCourse = (req, res) => {
  try {
    const id = req.params.id;
    courseService.updateFullCourse(req, res,id);
  } catch (error) {
    logger.error(error);
  }
};

findLiveSessions = async (req, res) => {
  try {
    let courses = await courseService.findLiveSessions(req, res);
    const id = req.body.studentId;

    let studentLessons = [];

    if (req.body.studentId) {
      studentLessons = await studentService.findStudentLessons(
        req,
        res,
        id,
        "other"
      );

      if (courses && courses.length > 0) {
        courses.forEach((_c) => {
          if (_c.chapters && _c.chapters.length > 0) {
            _c.chapters.forEach((_chap) => {
              if (_chap.lessons && _chap.lessons.length > 0) {
                _chap.lessons.forEach((_l) => {
                  if (studentLessons && studentLessons.length > 0) {
                    if (
                      studentLessons[0].lessons &&
                      studentLessons[0].lessons.length > 0
                    ) {
                      studentLessons[0].lessons.forEach((_sL) => {
                        if (_sL.lessonId.toString() === _l._id.toString()) {
                          _l.isOpen = true;
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });
      }
    }

    res.send({ courses });
  } catch (error) {
    logger.error(error);
  }
};

addStudentIntoCourse = (courseId, studentId) => {
  try {
    return courseService.addStudentIntoCourse(courseId, studentId);
  } catch (error) {
    logger.error(error);
  }
};

module.exports = {
  getAllData,
  create,
  findById,
  updateCourse,
  onSaleCourse,
  deleteCourse,
  getAllDeletedCourses,
  getAllDataPass,
  getCourseCalender,
  addFavoriteIntoCourse,
  removeCourseFavorite,
  removeCoursePackage,
  findTeacherCoursesByTeacherId,
  findByIdPass,
  findCourseByLessonIdPass,
  getNewReleasesCourses,
  getCourseCoreById,
  getStudentCourses,
  getExclusiveCourses,
  filterCourse,
  findStudentCourseById,
  findLiveSessions,
  updateFullCourse,
  addStudentIntoCourse,
};
