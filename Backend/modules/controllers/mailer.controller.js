const mailerService = require("../services/mailer.service");
const logger = require("../../helpers/logging");
const nodemailer = require("nodemailer");

createUser = (cb, type, res) => {
  let code = Math.floor(Math.random() * 10000) + 90000;
  try {
    let obj = {
      email: cb.email,
      userId: cb._id,
      userType: type,
      type: "Register",
      code: code,
    };
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "Support@darisly.com",
        pass: "bakbmmmyyvjfjung",
      },
    });
    transporter
      .sendMail({
        from: "Support@darisly.com", // sender address
        to: cb.email, // list of receivers
        subject: "Darisly To Verify Email", // Subject line
        html: `
      <p> Hi ${cb.firstName}</p>
      <p> If you would like to reset your password, </p>
      <p> please copy and paste the code into your app. </p>
      <p> Code : ${code}</p>
      <p> Darisly</p>
      `, // html body
      })
      .then((info) => {
        mailerService.createUser(obj, res);
        console.log({ info });
      })
      .catch(console.error);
  } catch (error) {
    logger.error(error);
  }
};

create = (req, res) => {
  try {
    let code = Math.floor(Math.random() * 10000) + 90000;
    req.body.code = code;
    req.body.type = "Forget";

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "Support@darisly.com",
        pass: "bakbmmmyyvjfjung",
      },
    });
    transporter
      .sendMail({
        from: "Support@darisly.com", // sender address
        to: req.body.email, // list of receivers
        subject: "Darisly To Forget Password", // Subject line
        html: `
      <p> Hi ${req.body.email}</p>
      <p> To complete Change your Password</p>
      <p> Code : ${code}</p>
      <p> Darisly</p>
      `, // html body
      })
      .then((info) => {
        console.log({ info });
        mailerService.create(req, res);
      })
      .catch(console.error);
  } catch (error) {
    logger.error(error);
  }
};

findMailer = (req, res) => {
  try {
    mailerService.findMailer(req.body, res, "send");
  } catch (error) {
    logger.error(error);
  }
};

checkMailer = (body, res) => {
  try {
    return mailerService.findMailer(body, res, "pass");
  } catch (error) {
    logger.error(error);
  }
};

// findMailer = async (req, res) => {
//   try {
//     let mailer = await mailerService.findMailer(req.body, res, 'send');
//     if (mailer && type == "send") {
//       res.status(200).send(mailer);
//     }
//   } catch (error) {
//     logger.error(error);
//   }
// };

// var transporter = nodemailer.createTransport({
//   service: "gmail",
//   auth: {
//     user: "youremail@gmail.com",
//     pass: "yourpassword",
//   },
// });

// var mailOptions = {
//   from: "youremail@gmail.com",
//   to: "myfriend@yahoo.com",
//   subject: "Verify Your Email Ya 3sal",
//   text: `Code :  '`,
// };

// transporter.sendMail(mailOptions, function (error, info) {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log("Email sent: " + info.response);
//   }
// });

module.exports = {
  findMailer,
  checkMailer,
  createUser,
  create,
};
