const Joi = require("@hapi/joi");
const notificationValidator = require("../validators/notification.validator");
const notificationService = require("../services/notification.service");
const courseController = require("./course.controller");
const logger = require("../../helpers/logging");

getAllData = (req, res) => {
  try {
    notificationService.findAll(req, res);
  } catch (error) {
    logger.error(error);
  }
};

create = (req, res) => {
  try {
    Joi.validate(
      req.body,
      notificationValidator.isValidNotification,
      (err, body) => {
        try {
          if (err) return res.status(422).send(err.details[0]);
          notificationService.create(req, res);
        } catch (error) {
          logger.error(error);
        }
      }
    );
  } catch (error) {
    logger.error(error);
  }
};

findById = (req, res) => {
  try {
    const id = req.params.id;
    notificationService.findById(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
updateNotification = (req, res) => {
  try {
    const id = req.params.id;
    notificationService.updateNotification(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
deleteNotification = (req, res) => {
  try {
    const id = req.params.id;
    notificationService.deleteNotification(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
getNotificationByUserId = (req, res) => {
  try {
    const id = req.params.id;
    notificationService.getNotificationByUserId(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
callbackGetNotificationByUserId = (id) => {
  try {
    return notificationService.callbackGetNotificationByUserId(id);
  } catch (error) {
    logger.error(error);
  }
};

newSessionEnrolledCourse = (req, course) => {
  // req.body = { ...req.body, "liveSession": true };
  // let course = await courseController.findCourseByLessonIdPass(req, res);
  // console.log(course);

  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";
  if (req.lesson) {
    let obj = {
      users: [{ userId: course.teacherId, type: "Teacher" }],
      reference: {
        type: "newSession",
        lessonId: req.lesson._id,
        courseId: course._id,
      },
    };

    obj.notificEn = `A new session has been added with the name (${req.lesson[toFound]}) in the (${course.courseName}) course`;
    obj.notificAr = `تم إضافة جلسة جديدة بإسم (${req.lesson[toFound]}) في كورس (${course.courseName})`;

    course.students.forEach((_s) => {
      obj.users.push({ userId: _s, type: "Student" });
    });

    notificationService.createCustom(obj);
  }
};

deliveredQuizTask = (req, obj, type) => {
  const lang = req.headers.lang ? req.headers.lang : "en";
  const toFound = lang === "en" ? "name" : "nameAr";

  let notific = {
    users: [{ userId: obj.teacherId, type: "Teacher" }],
  };

  if (type === "Quiz") {
    notific.notificEn = `A new Quiz has been delivered`;
    notific.notificAr = "تم تسليم إختبار جديد";
     notific.reference = { type: "deliveredQuiz", quizId: obj.quizId ,lessonId : obj.lessonId ,quizName : obj.quizName ,courseName : obj.courseName };

  } else if (type === "Task") {
    notific.notificEn = `A new Task has been delivered`;
    notific.notificAr = "تم تسليم مهمة جديدة";
    notific.reference = { type: "deliveredTask", taskId: obj.taskId ,lessonId : obj.lessonId ,taskName : obj.taskName ,courseName : obj.courseName };
  }

  notificationService.createCustom(notific);
};

module.exports = {
  getAllData,
  create,
  findById,
  updateNotification,
  deleteNotification,
  getNotificationByUserId,
  newSessionEnrolledCourse,
  callbackGetNotificationByUserId,
  deliveredQuizTask,
};
