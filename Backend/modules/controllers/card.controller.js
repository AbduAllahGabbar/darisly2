const Joi = require("@hapi/joi");
const cardValidator = require("../validators/card.validator");
const cardService = require("../services/card.service");
const logger = require("../../helpers/logging");

getAllData = (req, res) => {
  try {
    cardService.findAll(req, res);
  } catch (error) {
    logger.error(error);
  }
};

create = (req, res) => {
  try {
    // Validation
    Joi.validate(req.body, cardValidator.isValidCard, (err, body) => {
      try {
        if (err) return res.status(422).send(err.details[0]);
        cardService.create(req, res);
      } catch (error) {
        logger.error(error);
      }
    });
  } catch (error) {
    logger.error(error);
  }
};

findCardsByCourseId = (req, res, courseId) => {
  try {
    return cardService.findCardsByCourseId(req, res, courseId);
  } catch (error) {
    logger.error(error);
  }
};

findById = (req, res) => {
  try {
    const id = req.params.id;
    cardService.findById(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
updateCard = (req, res) => {
  try {
    const id = req.params.id;
    cardService.updateCard(req, res, id);
  } catch (error) {
    logger.error(error);
  }
};
deleteCard = (req, res) => {
  try {
    cardService.deleteCard(req, res);
  } catch (error) {
    logger.error(error);
  }
};
module.exports = {
  getAllData,
  create,
  findById,
  findCardsByCourseId,
  updateCard,
  deleteCard,
};
