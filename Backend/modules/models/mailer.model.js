const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const mailerSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  userId: {
    type: ObjectId,
  },
  userType: {
    type: String,
    enum: ["Student", "Teacher"],
    required: true,
  },
  type: {
    type: String,
    enum: ["Forget", "Register"],
    required: true,
  },
  code: {
    type: Number,
  },
});

const genericOperations = require("../genericService");
module.exports = {
  genericSchema: genericOperations(mongoose.model("mailer", mailerSchema)),
  defaultSchema: mongoose.model("mailer", mailerSchema),
};
