const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const mongooseAutopopulate = require("mongoose-autopopulate");

const courseSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  nameAr: {
    type: String,
    required: true,
  },
  hidden: {
    type: Boolean,
    default: false
  },
  subjectId: {
    type: ObjectId,
    ref: "subject",
    required: true,
  },
  educationSystemId: {
    type: ObjectId,
    ref: "educationSystem",
    required: true,
  },
  gradeId: {
    type: ObjectId,
    ref: "grade",
  },
  price: {
    type: Number,
    default: 0
  },
  pricePerLesson: {
    type: Number,
  },
  prerequisities: [
    {
      name: String,
    },
  ],
  onSale: [
    {
      packageId: {
        type: ObjectId,
      },
      expiryDate: { type: Date },
    },
  ],
  language: {
    type: String,
  },
  courseDescription: {
    type: String,
  },
  whatYouWillLearn: {
    type: String,
  },
  courseIcon: {
    type: String,
    required: true,
  },
  teacherId: {
    type: ObjectId,
    required: true,
    ref: "teacher",
  },
  courseIntro: {
    type: String,
    required: true,
  },
  product: {
    type: String,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  students: [
    {
      type: ObjectId,
      ref: "student",
      autopopulate: { select: "firstName lastName phoneNumber" },
    },
  ],
  favorites: [ObjectId],
  date: {
    type: Date,
    default: Date.now,
  },
  exclusive: {
    type: Boolean,
    default: false,
  },
  courseType: {
    type: String,
    enum: ["Educations", "Skills"],
    required: true,
  },
  status: {
    type: String,
    enum: ["Recorded", "Group", "Private"],
    required: true,
  },
});
const genericOperations = require("../genericService");
courseSchema.plugin(mongooseAutopopulate);

module.exports = {
  genericSchema: genericOperations(mongoose.model("course", courseSchema)),
  defaultSchema: mongoose.model("course", courseSchema),
};
