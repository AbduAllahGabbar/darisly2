const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const taskSchema = new Schema({
  taskId: {
    type: ObjectId,
    required: true
  },
  taskName: {
    type: String,
    required: true
  },
  courseName: {
    type: String,
    required: true
  },
  lessonId: {
    type: ObjectId,
    required: true
  },
  studentId: {
    type: ObjectId,
    required: true,
    ref: "student"
  },
  teacherId: {
    type: ObjectId,
    required: true
  },
  deliveredTask: {
    type: String,
    required: true
  },
  score: Number
});
taskSchema.index({ taskId: 1, studentId: 1 }, { unique: true });
const genericOperations = require("../genericService");
module.exports = {
  genericSchema: genericOperations(mongoose.model("delivered_task", taskSchema)),
  defaultSchema: mongoose.model("delivered_task", taskSchema),
};
