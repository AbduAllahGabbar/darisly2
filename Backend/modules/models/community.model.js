const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const communitySchema = new Schema({
  courseId: {
    type: ObjectId,
    required: true,
  },
  chapterId: {
    type: ObjectId,
    required: true,
  },
  lessonId: {
    type: ObjectId,
    required: true,
  },
  studentId: {
    type: ObjectId,
    required: true,
    ref: "student",
  },
  studentName: {
    type: String,
    required: true,
  },
  teacherId: {
    type: ObjectId,
    required: true,
  },
  teacherName: {
    type: String,
    required: true,
  },
  question: {
    date: {
      type: Date,
      default: Date.now,
    },
    title: {
      type: String,
    },
    attachments: [
      {
        type: {
          type: String,
          enum: ["Text", "Attachment", "Link", "Voice"],
          required: true,
        },
        value: {
          type: String,
          required: true,
        },
      },
    ],
  },
  replies: [
    {
      date: {
        type: Date,
        default: Date.now,
      },
      from: {
        type: String,
        enum: ["Teacher", "Student"],
        required: true,
      },
      title: {
        type: String,
      },
      attachments: [
        {
          type: {
            type: String,
            enum: ["Text", "Attachment", "Link", "Voice"],
            required: true,
          },
          value: {
            type: String,
            required: true,
          },
        },
      ],
    },
  ],
});
const genericOperations = require("../genericService");
module.exports = {
  genericSchema: genericOperations(
    mongoose.model("community", communitySchema)
  ),
  defaultSchema: mongoose.model("community", communitySchema),
};
