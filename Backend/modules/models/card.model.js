const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const mongooseAutopopulate = require("mongoose-autopopulate");

const cardSchema = new Schema({
  studentId: {
    type: ObjectId,
    required: true,
  },
  course: {
    type: ObjectId,
    required: true,
    ref: "course",
    autopopulate: { select: "name nameAr product price pricePerLesson" },
  },
  chapter: {
    type: ObjectId,
    ref: "chapter",
    autopopulate: { select: "name nameAr" },
  },
  lesson: {
    type: ObjectId,
    ref: "lesson",
    autopopulate: { select: "name nameAr product price" },
  },
  type: {
    type: String,
    required: true,
    enum: ["Course", "Lesson"],
  },
});
const genericOperations = require("../genericService");
cardSchema.plugin(mongooseAutopopulate);

module.exports = {
  genericSchema: genericOperations(mongoose.model("card", cardSchema)),
  defaultSchema: mongoose.model("card", cardSchema),
};
