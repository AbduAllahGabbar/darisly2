const boolean = require("@hapi/joi/lib/types/boolean");
const number = require("@hapi/joi/lib/types/number");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const notificationSchema = new Schema({
  users: [
    {
      notificId: Number,
      userId: {
        type: ObjectId,
        required: true,
      },
      type: {
        type: String,
        enum: ["Teacher", "Student"],
      },
      send: {
        type: Boolean,
        default: false,
      },
    },
  ],
  notificAr: {
    type: String,
    required: true,
  },
  notificEn: {
    type: String,
    required: true,
  },
  reference: {
    type: {
      type: String,
      enum: [
        "deliveredQuiz" ,
        "deadLineVideo" ,
        "deliveredTask",
        "deadLineTask" ,
        "sessionStart" ,
        "bookLesson" ,
        "newSession",
        "Admin"
    ],
    },
    lessonId: {
      type: ObjectId,
    },
    meetingId: {
      type: String,
    },
    meetingPassword: {
      type: String,
    },
    taskId: {
      type: ObjectId,
    },
    courseId: {
      type: ObjectId,
    },
    teacherId: {
      type: ObjectId,
    },
    quizId: {
      type: ObjectId,
    },
    leassonId: {
      type: ObjectId,
    },
    purchaseType: {
      type: String,
    },
    taskName: {
      type: String,
    },
    quizName: {
      type: String,
    },
    courseName: {
      type: String,
    }
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const genericOperations = require("../genericService");
module.exports = {
  genericSchema: genericOperations(
    mongoose.model("notification", notificationSchema)
  ),
  defaultSchema: mongoose.model("notification", notificationSchema),
};
