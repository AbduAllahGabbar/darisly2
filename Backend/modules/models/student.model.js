const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const mongooseAutopopulate = require("mongoose-autopopulate");

const studentSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    trim: true,
    minlength: 3,
    maxlength: 50,
  },
  lastName: {
    type: String,
    required: true,
    trim: true,
    minlength: 3,
    maxlength: 50,
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true,
  },
  password: {
    type: String,
  },
  verify: {
    type: Boolean,
    default: false,
  },
  socialMediaToken: {
    type: String,
  },
  phoneNumber: {
    type: String,
    required: true,
  },
  educationSystem: {
    type: ObjectId,
    ref: "educationSystem",
    required: true,
  },
  grade: {
    type: ObjectId,
    ref: "grade",
    required: true,
  },
  birthDate: Date,
  gender: {
    type: String,
    enum: ["male", "female"],
  },
  courses: [ObjectId],
  lessons: [
    {
      order: [
        {
          code: Number,
          price: Number,
        },
      ],
      deliveredTasks: [
        {
          type: ObjectId,
          ref: "delivered_task",
          autopopulate: true,
        },
      ],
      deliveredQuizzes: [
        {
          type: ObjectId,
          ref: "delivered_quiz",
          autopopulate: true,
        },
      ],
      // purchaseDates: [{
      //   type: Date,
      //   default: Date.now,
      // }],

      watchedVideos: [ObjectId],
      courseId: {
        type: ObjectId,
        required: true,
        ref: "course",
      },
      chapterId: {
        type: ObjectId,
        required: true,
      },
      lessonId: {
        type: ObjectId,
        required: true,
        ref: "lesson",
      },
      merchantOrderId: String,
      seen: {
        type: Boolean,
        default: false,
      },
      validFor: Number,
      startDate: Date,
      endDate: Date,
      sendNotific: {
        type: Boolean,
        default: false
      },
    },
  ],
  favorites: [ObjectId],
  myOrders: [
    {
      code: Number,
      purchaseCode: String,
      promoCode: {
        type: ObjectId,
        ref: "promoCode",
        autopopulate: { select: "promoCode discount value" },
      },
      coursesId : [ObjectId],
      type: {
        type: String,
        enum: ["Code", "CreditCart", "Aman", "Masary" , "VodafoneCash"],
      },
      totalPrice: Number,
      netPrice: Number,
      date: {
        type: Date,
        default: Date.now,
      },
      packages: [
        {
          type: ObjectId,
          ref: "package",
          autopopulate: { select: "courseId newPrice oldPrice" },
        },
      ],
    },
  ],
});

const genericOperations = require("../genericService");
studentSchema.plugin(mongooseAutopopulate);

module.exports = {
  genericSchema: genericOperations(mongoose.model("student", studentSchema)),
  defaultSchema: mongoose.model("student", studentSchema),
};
