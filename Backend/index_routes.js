const express = require("express");
const router = express.Router();
const passport = require("passport");

router.use(passport.initialize());
router.use(passport.session());

require("./helpers/passport")(passport);

const errorMiddleware = require("./helpers/error");

const routes = require("./modules/routes");
const courseRouter = require("./modules/routers/course.router");
const teacherAuthRouter = require("./modules/routers/teacherAuth.router");
const studentAuthRouter = require("./modules/routers/studentAuth.router");
const adminAuthRouter = require("./modules/routers/adminAuth.router");
const educationSystemRouter = require("./modules/routers/educationSystem.router");
const gradeRouter = require("./modules/routers/grade.router");
const paymentRouter = require("./modules/routers/payment.router");
const acceptanceRouter = require("./modules/routers/acceptance.router");
const cashRouter = require("./modules/routers/cash.router");
const previewRouter = require("./modules/routers/preview.router");
const trendingCourseRouter = require("./modules/routers/trendingCourse.router");
const vimeoRouter = require("./modules/routers/vimeo.router");
const subjectRouter = require("./modules/routers/subject.router");
const promotionRouter = require("./modules/routers/promotion.router");
const mailerRouter = require("./modules/routers/mailer.router");

router.use("/course", courseRouter);
router.use("/teacherAuth", teacherAuthRouter);
router.use("/studentAuth", studentAuthRouter);
router.use("/adminAuth", adminAuthRouter);
router.use("/educationSystem", educationSystemRouter);
router.use("/grade", gradeRouter);
router.use("/payment", paymentRouter);
router.use("/acceptance", acceptanceRouter);
router.use("/cash", cashRouter);
router.use("/preview", previewRouter);
router.use("/trendingCourse", trendingCourseRouter);
router.use("/vimeo", vimeoRouter);
router.use("/subject", subjectRouter);
router.use("/promotion", promotionRouter);
router.use("/mailer", mailerRouter);

router.use(
  passport.authenticate("jwt", {
    session: false,
  })
);

router.use("/", routes);

router.use(errorMiddleware);

module.exports = router;
