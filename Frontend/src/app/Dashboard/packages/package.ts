export class Package {
  courseId: string;
  subjectId: string;
  teacherId: string;
  educationSystemId: string;
  gradeId: string;
  course: any;
  chapter: any;
  packageLessons: any;
  withPromoCode: boolean;
  oldPrice: number;
  newPrice: number;
  expiryDate: Date;
}
