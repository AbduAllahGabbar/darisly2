import { OrderService } from '../services/order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Order } from './order';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {
  @ViewChild('closeButton') closeButton: ElementRef;
  allOrders = [];
  search = "";
  order = {};
  viewOrderModal = null;
  constructor(
    private orderService: OrderService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.getAllOrders();
  }

  getAllOrders(): any {
    this.spinner.show();
    this.orderService.getAllOrders().subscribe((result) => {      
      this.allOrders = result.myorders;
      this.spinner.hide();
    });
  }

  getFilterOrders(): any {
    if(!this.search){
      this.search = 'null'
    }
    
    this.spinner.show();
    this.orderService.getFilterOrders(this.search).subscribe((result) => {
      this.allOrders = result.myorders;
      this.search = ''
      this.spinner.hide();
    });
  }

  viewOrder(o, content): any {
    this.order = o;
    
    this.viewOrderModal = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
    });
  }

  callSwal(title, text, icon): any {
    return Swal.fire({
      title,
      text,
      icon,
      timer: 1500,
      showConfirmButton: false,
    });
  }
  reset(): any {
    this.order = new Order();
  }

  openUpdateOrder(order, content): any {
    this.orderService.getOrderCoreById(order._id).subscribe((result) => {
      this.order = result;
      console.log(this.order);
      
    });
    
    this.viewOrderModal = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
    });
  }
  closeContentModal(): any {
    this.viewOrderModal.close();
  }
}
