import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StudentsService {
  apiUrl = environment.apiUrl;
  link = 'api/student';

  constructor(private http: HttpClient) {}

  getAllStudents(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}`);
  }
}
