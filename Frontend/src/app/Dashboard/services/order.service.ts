import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  apiUrl = environment.apiUrl;
  link = 'api/grade';

  constructor(private http: HttpClient) {}

  getAllOrders(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/student/myOrders`);
  }
  getFilterOrders(filter): Observable<any> {

    return this.http.get<any>(`${this.apiUrl}/api/student/myOrdersSearch/${filter}`);
  }
  getAllOrdersWithoutPagination(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/student/myOrders?pageSize=20`);
  }
  getOrdersByEducSysId(id: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}/educationSystem/${id}`);
  }
  getOrderById(id: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}/${id}`);
  }
  getOrderCoreById(id: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}/core/${id}`);
  }
  deleteOrder(id: string): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/${this.link}/${id}`);
  }
  updateOrder(order): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/${this.link}/${order._id}`,
      order
    );
  }
  createOrder(order): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${this.link}/`, order);
  }
}
