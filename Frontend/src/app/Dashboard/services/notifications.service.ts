import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  apiUrl = environment.apiUrl;
  link = 'api/notification';

  constructor(private http: HttpClient) {}

  getAllNotification(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}`);
  }
  getNotificationByUserId(id: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${this.link}/${id}`);
  }
  deleteNotification(id: string): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/${this.link}/${id}`);
  }
  updateNotification(notification): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/${this.link}/${notification._id}`,
      notification
    );
  }

  createNotification(notification): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${this.link}/`, notification);
  }
}
