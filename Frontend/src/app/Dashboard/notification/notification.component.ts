import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CourseService } from '../services/course.service';
import { EducationSystemService } from '../services/education-system.service';
import { GradeService } from '../services/grade.service';
import { NotificationsService } from '../services/notifications.service';
import { StudentsService } from '../services/students.service';
import { SubjectService } from '../services/subject.service';
import { TeacherService } from '../services/teacher.service';
import { notifications } from './notifications';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
})
export class NotificationComponent implements OnInit {
  allTypes = ['Teachers', 'Students'];
  allCourseTypes = ['Educations', 'Skills'];

  userType: string;
  notification: notifications = new notifications();

  textSearch: any;
  notActiveCount: number = 0;
  allCourses: [{}];
  filterCours: any;

  AllTeachers: any;
  AllStudent: any[];
  studentsFilter: any[];

  AllGrades: any;
  allSubjects: any;

  allEducationSystems: any;
  allEduSysByCourseType: any;
  TeacherCourses = [];
  TeacherCoursesFilter: any[];

  SubjectCoursesFilter: any[];

  constructor(
    private Studentservice: StudentsService,
    private Teacherservice: TeacherService,
    private courseService: CourseService,
    private gradeService: GradeService,
    private subjectService: SubjectService,
    private educationSystemService: EducationSystemService,
    private notificationService: NotificationsService,
    private spinner: NgxSpinnerService
  ) {
    this.getAllCourses();
    this.getAllStudent();
    this.getAllTeacher();
    // this.getAllGrades();
    // this.getAllSubject();
    // this.getAllEducationSystems();
  }

  ngOnInit(): void {}
  callSwal(title, text, icon): any {
    return Swal.fire({
      title,
      text,
      icon,
      timer: 1500,
      showConfirmButton: false,
    });
  }
  reset(): any {
    this.notification = new notifications();
  }

  filterCourse(event) {
    console.log(event.target.id);

    // this.allCourses.filter((course: any) => {
    //   console.log(course.teacherId);
    //   if (course.teacherId == event.target.id) {
    //     console.log('success', course, event.target.id);
    //     this.filterCours == course;
    //   } else {
    //     console.log(course);
    //   }
    // });
  }
  getAllCourses(): any {
    this.spinner.show();
    this.courseService.getAllCourses().subscribe((result) => {
      this.notActiveCount = 0;
      result.forEach((_result) => {
        _result.$notActiveCount = 0;
        _result.chapters = _result.chapters || [];
        _result.chapters.forEach((_ch) => {
          _ch.lessons = _ch.lessons || [];
          _ch.lessons.forEach((_l) => {
            if (!_l.active && _l.liveSession) {
              if (new Date(_l.startDate) >= new Date()) {
                _result.$notActiveCount = _result.$notActiveCount + 1;
                this.notActiveCount = this.notActiveCount + 1;
              }
            }
          });
        });
      });
      this.allCourses = result;
      this.spinner.hide();
    });
  }

  /* function save all teacher course 
      and save it in TeacherCourses 
  */
  GetTeacherCoursesByTeacherId(TeacherId) {
    console.log(TeacherId);

    this.courseService
      .GetTeacherCoursesByTeacherId(TeacherId)
      .subscribe((Course: any) => {
        console.log(Course);

        this.TeacherCourses = Course;
      });
  }

  /* function return all teacher course
   * filtered by course Type and save it in TeacherCoursesFilter
   */
  GetTeacherCoursesByCourseType(courseType) {
    console.log(courseType);

    if (this.userType == 'Students') {
      this.TeacherCoursesFilter = this.TeacherCourses.filter((item) => {
        if (
          item.courseType == this.notification.courseType &&
          item.educationSystemId == this.notification.educationSystemId
        ) {
          if (this.notification.courseType == 'Educations') {
            if (item.gradeId == this.notification.gradeId) {
              return item;
            }
          } else {
            return item;
          }
        }
      });
    } else {
      this.TeacherCoursesFilter = this.allCourses.filter((item: any) => {
        if (
          item.courseType == this.notification.courseType &&
          item.educationSystemId == this.notification.educationSystemId
        ) {
          if (this.notification.courseType == 'Educations') {
            if (item.gradeId == this.notification.gradeId) {
              return item;
            }
          } else {
            return item;
          }
        }
      });
    }
  }

  /* function return all Subject depend on CourseId
   * filtered by course Type and save it in TeacherCoursesFilter
   */
  GetSubjectCoursesByCourseId(CourseId) {
    console.log(CourseId);

    this.SubjectCoursesFilter = this.TeacherCoursesFilter.filter((item) => {
      // console.log(item);

      if (item._id == this.notification.courseId) {
        console.log(';;;;', item);

        return item;
      }
    });
  }

  /**
   * get All teacher
   */
  getAllTeacher(): any {
    this.spinner.show();
    this.Teacherservice.getAllTeachers().subscribe((result) => {
      this.AllTeachers = result;
    });
  }
  /**
   * get all student
   * */
  getAllStudent(): any {
    this.spinner.show();
    this.Studentservice.getAllStudents().subscribe((result) => {
      this.AllStudent = result;
      console.log(result);
    });
  }

  /**
   * filterAllStudent function
   *  return student that have same value of
   *  educationSystemId , gradeId , courseName
   */
  filterAllstudents() {
    // this.studentsFilter = [];
    this.AllStudent.filter((item) => {
      if (
        this.notification.educationSystemId.indexOf(item.educationSystem) !=
          -1 &&
        this.notification.educationSystemId != null &&
        this.notification.educationSystemId != ''
      ) {
        console.log(item);
        this.studentsFilter.push(item);
      }
      if (
        this.notification.gradeId.indexOf(item.grade) != -1 &&
        this.notification.gradeId != null &&
        this.notification.gradeId != ''
      ) {
        console.log(item);

        this.studentsFilter.push(item);
      }
      if (
        this.notification.courseId.indexOf(item.courses) != -1 &&
        this.notification.courseId != null &&
        this.notification.courseId != ''
      ) {
        console.log(item);
        this.studentsFilter.push(item);
      }
    });
  }

  getAllGrades(): any {
    this.spinner.show();
    this.gradeService.getAllGrades().subscribe((result) => {
      this.AllGrades = result;
    });
  }
  getAllSubject() {
    this.spinner.show();
    this.subjectService.getAllSubjects().subscribe((result) => {
      this.allSubjects = result;
    });
  }
  getAllEducationSystems(): any {
    this.spinner.show();
    this.educationSystemService.getAllEducationSystems().subscribe((result) => {
      this.allEducationSystems = result;
      this.spinner.hide();
    });
  }
  getAllEduSysByCourseType(courseType): any {
    this.spinner.show();

    this.educationSystemService
      .getAllEduSysByCourseType(courseType)
      .subscribe((result) => {
        this.allEduSysByCourseType = result;
        this.spinner.hide();
      });
  }
  getAllGradesByEduSysId(educationSystemId): any {
    this.spinner.show();
    this.gradeService
      .getGradesByEducSysId(educationSystemId)
      .subscribe((result) => {
        this.AllGrades = result;
        this.spinner.hide();
      });
  }
  getAllSubjectsByEduSysId(educationSystemId): any {
    this.spinner.show();
    this.subjectService
      .getSubjectsByEducSysId(educationSystemId)
      .subscribe((result) => {
        this.allSubjects = result;
        this.spinner.hide();
      });
  }

  eventCheckBox(event) {

    if (event.attributes.id.nodeValue == 'allStudent') {
      console.log(
        'allStudent',
        event.attributes.id,
        'allStudent',
        event.checked,
        this.notification.IsAllStudent
      );
    }
    if (event.attributes.id.nodeValue == 'allTeacher') {
      console.log(
        'tea',
        event.attributes.id,
        'te',
        event.checked,
        this.notification.IsAllTeacher
      );
    }
  }

  createNotification() {
    let notific = {
      users: [],
      reference : { type :"Admin"},
      notificAr:this.notification.notificAr,
      notificEn:this.notification.notificEn
  };

      if (this.notification.IsAllStudent || this.notification.IsAllTeacher){

        if (this.notification.IsAllStudent) {

          this.AllStudent.forEach(_t => {
            notific.users.push({
              userId : _t._id,
              type : "Student",
            })
          });
        }
        if (this.notification.IsAllTeacher) {
        
          this.AllTeachers.forEach(_t => {
            notific.users.push({
              userId : _t._id,
              type : "Teacher",
            })
          });

        }
      } else {
        if (this.userType == 'Teachers') {
          this.AllTeachers.forEach((item) => {
            console.log(item);
          });
        } else if (this.userType == 'Students') {
          this.studentsFilter.forEach((item) => {
            console.log(item);
          });
    
        }
      }



    this.notificationService
      .createNotification(notific)
      .subscribe((res) => {
        this.callSwal('Created !', '"Data created successfully', 'success');
        this.spinner.hide();
        this.reset();
        this.getAllGrades();
      });
  }
}
