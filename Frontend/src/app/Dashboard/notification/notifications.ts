export class notifications {
  users: [
    {
      send: boolean;
      _id: string;
      userId: string;
      type: string;
    }
  ];
  reference: {};
  notificEn: string;
  notificAr: string;

  IsAllStudent: boolean; //for All student in All Course Type
  IsAllTeacher: boolean; //for All Teacher in All Course Type
  subjectId: any;
  courseId: any;
  gradeId: any;
  educationSystemName: string;
  educationSystemId: string;

  courseType: string;
  teacherId: any;
  studentId: any;
  courseName: any;
}
