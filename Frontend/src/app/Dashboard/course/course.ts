export class Course {
  name: string;
  nameAr: string;
  hidden: boolean;
  subjectId: string;
  gradeId: string;
  educationSystemId: string;
  courseType: string;
  status: string;
  product: string;
  price: number;
  pricePerLesson: number;
  courseIcon: string;
  courseIntro: string;
  teacherId: string;
  prerequisities: [{}];
  language: string;
  courseDescription: string;
  whatYouWillLearn: string;
}
