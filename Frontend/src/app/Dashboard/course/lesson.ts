export class Lesson {
  chapterId: string;
  name: string;
  nameAr: string;
  price: number;
  active: boolean;
  liveSession: boolean;
  validFor: number;
  product: string;
  liveType: string;
  maxNumStu: number;
  startDate: Date;
  startTime: string;
  endDate: Date;
  endTime: string;
  meetingId: string;
  meetingPassword: string;
  notes: string;
}
