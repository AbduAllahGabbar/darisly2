export class Teacher {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  gender: string;
  specialization: string;
  bio: string;
  nationality: string;
  frontIdCard: string;
  backIdCard: string;
  certificatesImage: [string];
  certificates: [{}];
  educations: [{}];
  personalImage: string;
  approved: boolean;
}
