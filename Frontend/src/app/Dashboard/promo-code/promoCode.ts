export class PromoCode {
  promoCode: string;
  discount: string;
  value: number;
  expiryDate: Date;
}
